\startonoddpage
\section{The C memory model}
\label{sec:memory-model}

This section covers
\begin{itemize}
\item Understanding object representations
\item Working with untyped pointers and casts
\item Restricting object access with \plainindex[effective type]{effective types} and alignment
\end{itemize}

\bigskip%
\noindent%
Pointers present us with a certain abstraction of the environment and state
in which our program is executed, the \emphindex[memory model]{C memory
  model}. We
may apply the unary operator \code{&} to (almost) all objects\footnote{Only
  objects that are declared with keyword \code{register} don't have an
  address; see subsection~\smartref{sec:automatic} in
  level~\ref{level:jay}} to retrieve their address and use it to inspect and
change the state of our execution.

This access to objects via pointers is still an abstraction because, in C, no distinction of the ``real'' location of an object is made. It
could reside in your computer's RAM, on a disk file, or correspond to an IO
port of a temperature sensor on the moon; you shouldn't care. C is
supposed to do the right thing, regardless.

And, indeed, on modern operating systems, all you get via pointers is
something called \emphindex[memory!virtual]{virtual memory},
basically a fiction that maps the \emphindex{address space} of your process
to physical memory addresses of your machine. All this was invented to
ensure certain properties of your program executions:
\begin{description}
\item[portable] You do not have to care about physical memory addresses on
  a specific machine.
\item[safe] Reading or writing virtual memory that your process does not
  own will affect neither your operating system nor any other process.
\end{description}
The only thing C must care about is the \emph{type} of the object
a pointer addresses. Each pointer type is derived from another
type, its base type, and each such derived type is a distinct new
type.
\begin{reminder}
  Pointer types with distinct base types are distinct.
\end{reminder}

In addition to providing a virtual view of physical memory, the memory model also
simplifies the view of objects themselves. It makes the assumption
that each object is a collection of bytes, the \emphindex{object
  representation} (subsection~\ref{sec:uniform-memory-model});%
\footnote{%
  The object representation is related to but not the same thing as the
  \emph{binary representation} that we saw in
  subsection~\ref{sec:binary-representation}.%
} %
see figure~\ref{fig:memory-levels} for a schematic view.
A convenient tool to inspect that object representation is
\emphindex[union]{unions} (subsection~\ref{sec:unions}). Giving direct access
to the object representation (subsection~\ref{sec:memory-state}) allows us to do
some fine-tuning. On the other hand, it also opens the door to unwanted
or conscious manipulations of the state of the abstract machine:
\index{state!abstract}%
\index{abstract state}%
tools for
that are untyped pointers (subsection~\ref{sec:void_pointer}) and casts
(subsection~\ref{sec:expl-conv-casts}). \plainindex[effective type]{Effective types}
(subsection~\ref{sec:effective-type}) and alignment
(subsection~\ref{sec:alignment}) describe formal limits and platform
constraints for such manipulations.

\begin{figure}
  \begin{center}
    \hspace*{-3em}
    \begin{tabular}[t]{rcccccccc}
      Semantic type&\multicolumn{4}{c}{\small\fbox{\ \ \code{int32_t}\ \ \ \ \ %
                  $-b_{31}2^{31} + \sum\limits_{i=0}^{30}b_{i}2^{i}$}}\\
           &\multicolumn{4}{c}{$\uparrow$}\\
           &\multicolumn{4}{c}{\small\code{typedef}}\\
           &\multicolumn{4}{c}{$\downarrow$}\\
      Basic type&\multicolumn{4}{c}{\small\fbox{\ \ \code{signed int}\ \ \ \ \ %
                  $-b_{31}2^{31} + \sum\limits_{i=0}^{30}b_{i}2^{i}$}}\\
           &\multicolumn{4}{c}{$\uparrow$}\\
           &\multicolumn{4}{c}{sign representation}\\
           &\multicolumn{4}{c}{$\downarrow$}\\
      Binary representation&\tiny$\arraybox{[3]}{b_{31}\cdots b_{24}}$&\tiny$\arraybox{[2]}{b_{23}\cdots b_{16}}$&\tiny$\arraybox{[1]}{b_{15}\cdots b_{8}}$&\tiny$\arraybox{[0]}{b_{7}\cdots b_{0}}$\\
           &\multicolumn{4}{c}{$\uparrow$}\\
           &\multicolumn{4}{c}{endianness}\\
           &\multicolumn{4}{c}{$\downarrow$}\\
      Object representation&\tiny$\arrayel{[0]}{unsigned char}$&\tiny$\arrayel{[1]}{unsigned char}$&\tiny$\arrayel{[2]}{unsigned char}$&\tiny$\arrayel{[3]}{unsigned char}$\\
      \small\code{unsigned char[4]}&$\updownarrow$&$\updownarrow$&$\updownarrow$&$\updownarrow$\\
      Storage instance&\tiny$\arraybox{+0}{byte}$&\tiny$\arraybox{+1}{byte}$&\tiny$\arraybox{+2}{byte}$&\tiny$\arraybox{+3}{byte}$\\
      \small\code{void*}&$\updownarrow$&$\updownarrow$&$\updownarrow$&$\updownarrow$\\
      OS/physical memory&\tiny$\arraybox{+0}{byte}$&\tiny$\arraybox{+1}{byte}$&\tiny$\arraybox{+2}{byte}$&\tiny$\arraybox{+3}{byte}$\\
    \end{tabular}\\[2ex]
  \caption{The different levels of the value-memory model for an
    \code{int32_t}. Example of a platform that maps this type to a 32-bit
    \code{signed} \tcode{int} and uses a
    little-endian object representation.}
  \end{center}
  \label{fig:memory-levels}
  \index{little-endian}
  \index{object representation}
  \index{sign representation}
  \index{endian!little}
  \index{representation!object}
  \index{representation!sign}
\end{figure}

\subsection{A uniform memory model}
\label{sec:uniform-memory-model}
{\lstindexbf
  \label{sec:sizeof-para}
Even though generally all objects are typed, the memory model makes
another simplification: all objects are an assemblage of
\jargonindex{bytes}. The \code{sizeof} operator that we introduced
in the context of arrays measures the size of an object in terms of the
bytes that it uses.
}
There are three distinct types that, by definition, use
exactly 1 byte of memory: the character types \code{char}, \code{unsigned
  char}, and \code{signed} \tcode{char}.
\begin{reminder}
  \texttt{sizeof(char)} is \texttt{1} by definition.%
  \label{rem:sizeof-char-1}%
\end{reminder}

Not only can all objects be ``accounted'' in size as character types on a
% AQ I don't understand what you mean by "accounted" in this context.
% AA: that we compute the size of objects by the amount of characters that
% are needed to represent them. I don't know how to state that better.
lower level, they can even be inspected and manipulated as if they were
arrays of such character types. A little later, we will see how this can be
achieved, but for the moment we will just note the following:

\begin{reminder}
  Every object \code{A} can be viewed as \code{unsigned} \tcode{char[sizeof A]}.
\end{reminder}

\begin{reminder}
  Pointers to character types are special.
\end{reminder}

\historic
Unfortunately, the types that are used to compose all other object
types are derived from \code{char}, the type we looked at for
the characters of strings. This is merely a historical accident, and you
shouldn't read too much into it. In particular, you should clearly
distinguish the two different use cases.

\begin{reminder}
  Use the type \code{char} for character and string data.
\end{reminder}

\begin{reminder}
  Use the type \code{unsigned} \tcode{char} as the atom of all object types.
\end{reminder}

The type \code{signed} \tcode{char} is of much less importance than the two others.

As we have seen, the \code{sizeof} operator counts the size of an object in terms
of how many \code{unsigned} \tcode{char}\/s it occupies.

\begin{reminder}
  The \code{sizeof} operator can be applied to objects and object types.
\end{reminder}

From the previous discussion, we can also distinguish two syntactic variants for
\code{sizeof}: with and without parentheses. Whereas the syntax for an
application to objects can have both forms, the syntax for types needs
parentheses.

\begin{reminder}
  The size of all objects of type \code{T} is given by \code{sizeof(T)}.
\end{reminder}


% \subsection{\code{unsigned} \tcode{char} is special}
% \label{sec:unsigned-char-spec}

\subsection{Unions}
\label{sec:unions}

{\lstindexbf
Let us now look at a way to examine the individual bytes of objects. Our
preferred tool for this is the \code{union}.
}
These are similar in declaration to \code{struct} but have different
semantics:
\lstextract{4}{9}{code/endianness.c}

The difference here is that such a \code{union} doesn't collect objects of
different types into one bigger object, but rather \emph{overlays} an object
with several different type interpretations. That way, it is the perfect tool to
inspect the individual bytes of an object of another type.

Let us first try to figure out what values we would expect for the
individual bytes. In a slight abuse of language, let us speak of the
different parts of an unsigned number that correspond to the bytes as
\emph{representation digits}. Since we view the bytes as being of type
\code{unsigned} \tcode{char}, they can have values \code{0} \ldots \code{UCHAR_MAX},
inclusive, and thus we interpret the number as written with a base of
\code{UCHAR_MAX+1}. In the example, on my machine, a value of type
\code{unsigned} can be expressed with \code{sizeof(unsigned) == 4} such
representation digits, and I chose the values \code{0xAA}, \code{0xBB},
\code{0xCC}, and \code{0xDD} for the highest- to lowest-order representation
digit. The complete \code{unsigned} value can be computed using the following
expression, where \code{CHAR_BIT} is the number of bits in a character
type:
\begin{lstlisting}
((0xAA << (CHAR_BIT*3))
    |(0xBB << (CHAR_BIT*2))
    |(0xCC << CHAR_BIT)
    |0xDD)
\end{lstlisting}

With the \code{union} defined earlier, we have two different facets to look
at the same \code{twofold} object: \code{twofold.val} presents it as being
an \code{unsigned}, and \code{twofold.bytes} presents it as an array of
\code{unsigned} \tcode{char}. Since we chose the length of \code{twofold.bytes} to
be exactly the size of \code{twofold.val}, it represents exactly its bytes
and thus gives us a way to inspect the \jargonindex{object
  representation} of an \code{unsigned} value using all its
representation digits:
\lstextract{12}{14}{code/endianness.c}

On my computer, I receive a result as shown
here:\footnote{Test the code on your own machine.}

\begin{terminal}
~/build/modernC% code/endianness
value is 0xAABBCCDD
byte[0]: 0xDD
byte[1]: 0xCC
byte[2]: 0xBB
byte[3]: 0xAA
\end{terminal}

For my machine, we see that the output has the low-order
representation digits of the integer first, then the next-lower order
digits, and so on. At the end, the highest-order digits are printed. So the
in-memory representation of such an integer on my machine has the
low-order representation digits before the high-order ones.

This is \emph{not} normalized by the standard but is an \plainindex{implementation-defined}
behavior.
\begin{reminder}
  The in-memory order of the representation digits of an arithmetic type is
  implementation defined.
\end{reminder}

That is, a platform provider might decide to provide a storage order that
has the highest-order digits first and then print lower-order digits one by
one. The storage order, the \jargonindex{endianness}, as given for my
machine, is called \jargonindex[endian!little]{little-endian}.
A system that has high-order representation digits first is called
\jargonindex[endian!big]{big-endian}.%
\footnote{%
  The names are derived from the fact that the big or small ``end'' of a
  number is stored first.%
} %
Both orders are commonly used by modern processor types. Some processors
are even able to switch between the two orders on the fly.

Since \C{23}, the header \Cinclude{stdbit.h} has macros that provide the
endianess that the compiler implements:
{\lstindexbf
\begin{center}
  \begin{tabular}{l|l}
    platform endianess & \code{__STDC_ENDIAN_NATIVE__} \\
    \hline
    little             & \code{__STDC_ENDIAN_LITTLE__}\\
    big                & \code{__STDC_ENDIAN_BIG__}  \\
    other              & different from above
  \end{tabular}
\end{center}
}

The previous output also shows another \plainindex{implementation-defined} behavior: I used
the feature of my platform that one representation digit can be
printed nicely by using two hexadecimal digits.
{\lstindexbf
In other words, I assumed that
\code{UCHAR_MAX+1} is \code{256} and that the number of value bits in an
\code{unsigned} \tcode{char}, \code{CHAR_BIT}, is \code{8}. Again, this is
implementation-defined behavior. Although the vast majority of platforms
have these properties,\footnote{In particular, all POSIX systems.} there are
still some around that have wider character types.
}
\begin{reminder}
  On most architectures, \code{CHAR_BIT} is \code{8} and \code{UCHAR_MAX} is
  \code{255}.
\end{reminder}

In the example, we have investigated the in-memory representation of the
simplest arithmetic base types, unsigned integers. Other base types have
in-memory representations that are more complicated: signed integer types
have to encode the sign; \plainindex{floating-point} types have to encode the sign, mantissa,
and exponent; and pointer types may follow any internal
convention that fits the underlying architecture.\footwarm{Design a similar
  \code{union} type to investigate the bytes of a pointer type, such as
  \code{double*}.}\footwarm{With such a \code{union}, investigate the
  addresses of two consecutive elements of an array.}\footwarm{Compare the
  addresses of the same variable between different executions.}

% conversion needs explicit cast: 6.5.4 p3

\subsection{Memory and state}
\label{sec:memory-state}
\index{state!abstract}%
\index{abstract state}%
The value of all objects constitutes the state of the abstract state
machine and thus the state of a particular execution. C's memory model
provides something like a unique location for (almost) all objects through
the \code{&} operator, and that location can be accessed and modified from
different parts of the program through pointers.

Doing so makes the determination of the abstract state of an execution much
more difficult, if not impossible in many cases:
\begin{lstlisting}
double blub(double const* a, double* b);

int main(void) {
  double c = 35;
  double d = 3.5;
  printf("blub is %g\n", blub(&c, &d));
  printf("after blub the sum is %g\n", c + d);
}
\end{lstlisting}

Here, we (as well as the compiler) only see a declaration of function
\code{blub}, with no definition. So we cannot conclude much about what that
function does to the objects its arguments point to. In particular, we
don't know if the variable \code{d} is modified, so the sum \code{c
  + d} could be anything. The program really has to inspect the object
\code{d} in memory to find out what the values \emph{after} the call
to \code{blub} are.

Now let us look at such a function that receives two pointer
arguments:
\begin{lstlisting}
double blub(double const* a, double* b) {
  double myA = *a;
  *b = 2*myA;
  return *a;      // May be myA or 2*myA
}
\end{lstlisting}

Such a function can operate under two different assumptions. First, if
called with two distinct addresses as arguments, \code{*a} will be
unchanged, and the return value will be the same as \code{myA}. But if
both argument are the same, such as if the call is \code{blub(\&c, \&c)}, the
assignment to \code{*b} will change \code{*a}, too.

The phenomenon of accessing the same object through different pointers is
called \jargonindex{aliasing}; it is a common cause for missed
optimization.  In both cases, either that two pointers always alias or that
they never alias, the abstract state of an execution is much reduced, and
the optimizer often can take much advantage of that knowledge.
\index{state!abstract}%
\index{abstract state}%
Therefore, C forcibly restricts the possible
aliasing to pointers of the same type.
\begin{reminder}[Aliasing]
  With the exclusion of character types, only pointers of the same
  base type may alias.
  \label{rem:aliasing}
\end{reminder}

To see this rule in effect, consider a slight modification of our
previous example:
\begin{lstlisting}
size_t blob(size_t const* a, double* b) {
  size_t myA = *a;
  *b = 2*myA;
  return *a;      // Must be myA
}
\end{lstlisting}

Because here the two parameters have different types, C \emph{assumes} that
they don't address the same object. In fact, it would be an error to call
that function as \code{blob(&e, &e)} (for some variable \code{e}), since
this would never match the prototype of \code{blob}. So at the
\code{return} statement, we can be sure that the object \code{*a} hasn't
changed and that we already hold the needed value in variable \code{myA}.

There are ways to fool the compiler and to call such a function with a
pointer that addresses the same object. We will see some of these
cheats later. Don't do this; it is a road to much grief and
despair. \emph{If} you do so, the behavior of the program becomes
undefined, so you have to guarantee (prove!) that no aliasing takes
place.
\index{behavior!undefined}%
\index{undefined behavior}%

On the contrary, we should try to write our programs so they protect our
variables from ever being aliased, and there is an easy way to achieve
that.
\begin{reminder}
  Avoid the \code{&} operator.
  \label{rem:avoid-addressof}
\end{reminder}
Depending on the properties of a given variable, the compiler may see that
the address of the variable is never taken, and thus the variable
can't alias at all. In subsection~\ref{sec:lifetime}, we will see which
properties of a variable or object may influence such decisions and
how the \code{register} keyword can protect us from taking addresses
inadvertently.
Later, in subsection~\ref{sec:avoid-alias-restrict}, we will see how the \code{restrict} keyword
allows us to specify aliasing properties of pointer arguments, even if they
have the same base type.

\subsection{Pointers to unspecific objects}
\label{sec:void_pointer}

As we have seen, the object representation provides a view of an
object \code{X} as an array \code{unsigned} \tcode{char[sizeof X]}. The starting
address of that array (of type \code{unsigned} \tcode{char*}) provides access to
memory that is stripped of the original type information.
\index{void@\code{void}!pointer|textbf}
\index{pointer!to void@to \code{void}|textbf}

C has invented a powerful tool to handle such pointers more
generically. These are pointers to a sort of \emph{non-type},
\code{void}.
\begin{reminder}
  Any object pointer converts to and from \code{void*}.
\end{reminder}

Note that this only talks about object pointers, not function
pointers. Think of a \code{void*} pointer that holds the address of an
existing object as a pointer into a \emphindex{storage instance} %
\index{storage|textbf}%
that holds the object; see figure~\ref{fig:memory-levels} at page~\pageref{fig:memory-levels}. As an analogy for
such a hierarchy, you could think of entries in a phone book:
a person's name corresponds to the identifier that refers to an object;
their categorization with a
``mobile,'' ``home,'' or ``work'' entry corresponds to a type;
and their phone number itself is some sort of address (in which, by itself,
you typically are not interested).
%
But then, even the phone number abstracts away from the specific
information of where the other phone is located (which would be the storage
instance underneath the object) or specific information about the other
phone itself (for example, if it is on a landline or the mobile network) and
what the network has to do to actually connect you to the person at the
other end.

\begin{reminder}
  An object has storage, type, and value.
\end{reminder}

Not only is the conversion to \code{void*} well defined, but it
also is guaranteed to behave well with respect to the pointer value.
\index{behavior!defined}%
\index{defined behavior}%
\index{state!defined}%
\index{defined state}%

\begin{reminder}
  Converting an object pointer to \code{void*} and then back to the
  same type is the identity operation.
\end{reminder}
So, the only thing that we lose when converting to \code{void*} is the
type information; the value remains intact.


\begin{reminder}[a$void^2$*]
  A\emph{void} \code{void*}.
\end{reminder}
It completely removes any type information that is associated with an
address. Avoid it whenever you can. The other way around is much less
critical---in particular, if you have a C library call that returns a
\code{void*}.
\index{void@\code{void}!pointer}
\index{pointer!to void@to \code{void}}

\code{void} as a type by itself shouldn't be used for variable
declarations since it won't lead to an object with which we can
do anything.

\subsection{Explicit conversions}
\label{sec:expl-conv-casts}
A convenient way to look at the object representation of object \code{X}
would be to somehow convert a pointer to \code{X} to a pointer of type
\code{unsigned} \tcode{char*}:
\begin{shortlisting}
  double X;
  unsigned char* Xp = &X; // error: implicit conversion not allowed
\end{shortlisting}
Fortunately, such an implicit conversion of a \code{double*} to
\code{unsigned} \tcode{char*} is not allowed. We would have to make this conversion
somehow explicit.

We already have seen that in many places, a value of a certain type is
implicitly converted to a value of a different type
(subsection~\ref{sec:conversions}), and that narrow integer types are first
converted to \code{int} before any operation.
In view of that, narrow types only make sense in very special
circumstances:
\begin{itemize}
\item You have to save memory. You need to use a really big array of small
  values. \emph{Really big} here means potentially millions or billions. In such a
  situation, storing these values may gain you something.
\item You use \code{char} for characters and strings. But then you wouldn't
  do arithmetic with them.
\item You use \code{unsigned} \code{char} to inspect the bytes of an
  object. But then, again, you wouldn't do arithmetic with them.
\end{itemize}

\medskip%
\noindent%
Conversions of pointer types are more delicate because they can change the
type interpretation of an object. Only two forms of implicit
conversions are permitted for data pointers: conversions from and to
\code{void*} and conversions that add a qualifier to the target type. Let's
look at some examples:
\begin{lstlisting}
float f = 37.0;        // Conversion: to float
double a = f;          // Conversion: back to double
float* pf = &f;        // Exact type
float const* pdc = &f; // Conversion: adding a qualifier
void* pv = &f;         // Conversion: pointer to void*
float* pfv = pv;       // Conversion: pointer from void*
float* pd = &a;        // Error: incompatible pointer type
double* pdv = pv;      // Error if used
\end{lstlisting}
\index{behavior!undefined}%
\index{undefined behavior}%
The first two conversions that use \code{void*} (\code{pv} and \code{pfv})
are already a bit tricky: we convert a pointer back and forth, but we watch
that the target type of \code{pfv} is the same as \code{f} so everything
works out fine.

Then comes the erroneous part. In the initialization of \code{pd}, the
compiler can protect us from a severe fault. Assigning a pointer to a type
that has a different size and interpretation can and will lead to serious
damage. Any conforming compiler \emph{must} give a diagnosis for this
line. By now, you should understand well that your code should not produce
compiler warnings (\mbox{takeaway~\ref{rem:no-warnings}}), and you know that you should
not continue until you have repaired such an error.

The last line is worse: it has an error, but that error is syntactically
correct. The reason this error might go undetected is that our first
conversion for \code{pv} has stripped the pointer from all type
information. So, in general, the compiler can't know what type of object is
behind the pointer.

In addition to the implicit conversions that we have seen until now, C also
allows us to convert explicitly using \jargonindex{casts}.\footnote{A
  cast of an expression \code{X} to type \code{T} has the form
  \code{(T)X}. Think of it like ``\emph{to cast a spell}.''}
With a cast, you are telling the compiler that you know better than it does and that the
type of the object behind the pointer is not what it thinks, so it should
shut up. In most use cases that I have come across in real life, the
compiler was right, and the programmer was wrong. Even experienced
programmers tend to abuse casts to hide poor design decisions concerning
types.
\begin{reminder}
  Don't use casts.
\end{reminder}

They deprive you of precious information, and if you choose your types
carefully, you will only need them for very special occasions.

One such occasion is when you want to inspect the contents of an object on the
byte level. Constructing a \code{union} around an object, as we saw in
subsection~\ref{sec:unions}, might not always be possible (or may be too complicated),
so here we can go for a cast:
\lstextract{15}{18}{code/endianness.c}

In that direction (from ``pointer to object'' to a ``pointer to character
type''), a cast is mostly harmless.%
\footnote{%
  Note, though, that even a recent compiler at the time of this writing gets
  that particular code snippet wrong and is convinced that the byte-wise
  access goes to uninitialized data. Avoid casts as far as you may.%
}%

\subsection{Effective types}
\label{sec:effective-type}

To cope with different views of the same object that pointers may provide,
C has introduced the concept of \emphindex[effective type]{effective types}. It heavily
restricts how an object can be accessed.
\begin{reminder}[Effective type]
  Objects must be accessed through their effective type\\
  or through a pointer to a character type.
  \label{rem:effective-type}
\end{reminder}

Because the effective type of a \code{union} variable is the \code{union}
type and none of the member types, the rules for \code{union} members can
be relaxed.
\begin{reminder}
  Any member of an object that has an effective \code{union} type can be
  accessed at any time, provided the byte representation amounts to a valid
  value of the access type.
\end{reminder}

For all objects we have seen so far, it is easy to determine the
\plainindex[effective type]{effective type}.
\begin{reminder}
  The effective type of a variable or compound literal is the type of its
  declaration.
\end{reminder}

Later, we will see another category of objects that are a bit more
involved.

Note that this rule has no exceptions, and we can't change the type of
such a variable or compound literal.
\begin{reminder}
  Variables and compound literals must be accessed through their declared
  type or through a pointer to a character type.
\end{reminder}

Also, observe the asymmetry in all of this for character types. Any object
can be seen as being composed of \code{unsigned} \tcode{char}, but no array of
\code{unsigned} \tcode{char}\/s can be used through another type:
\begin{shortlisting}
  unsigned char A[sizeof(unsigned)] = { 9 };
  // Valid but useless, as most casts are
  unsigned* p = (unsigned*)A;
  // Error: access with a type that is neither the effective type nor a
  // character type
  printf("value \%u\n", *p);
\end{shortlisting}
\label{expl:char-to-unsigned}

Here, the access \code{*p} is an error, and the program state is undefined
afterward. This is in strong contrast to our dealings with \code{union}
earlier: see subsection~\ref{sec:unions}, where we actually could view a byte
sequences as an array of \code{unsigned} \tcode{char} or \code{unsigned}.
\index{behavior!undefined}%
\index{undefined behavior}%

The reasons for such a strict rule are multiple. The very first motivation
for introducing \plainindex[effective type]{effective types} in the C standard was to deal with aliasing,
as we saw in subsection~\ref{sec:memory-state}. In fact, the aliasing rule
(\mbox{takeaway~\ref{rem:aliasing}}) is derived from the effective type rule
(\mbox{takeaway~\ref{rem:effective-type}}). As long as there is no \code{union}
involved, the compiler knows that we cannot access a \code{double} through
a \code{size_t*}, and so it may \emph{assume} that the objects are
different.

\subsection{Alignment}
\label{sec:alignment}

The inverse direction of pointer conversions (from ``pointer to character type'' to ``pointer to
object'') is not harmless at all and not only because of possible aliasing.
This has to do with another property of
C's memory model: \jargonindex{alignment}. Objects of most noncharacter
types can't start at any arbitrary byte position; they usually start at a
\jargonindex{word boundary}. The alignment of a type then describes the
possible byte positions at which an object of that type can start.

If we force some data to a false alignment, really bad things can
happen. To see that, have a look at the following code:
\lstextract{11}{33}{code/crash.c}
%\lstinputlisting{code/crash.c}

This starts with a declaration of a \code{union} similar to what we saw
earlier. Again, we have a data object (of type \code{complex}
\code{double[2]} in this case) that we overlay with an array of
\code{unsigned} \code{char}.
%
The obvious intent of this program is to print one output line per loop
execution, each prefixed with the value of \code{offset}.
%
Other than the fact that this part is a bit more complex,
at first glance there is no major problem with it. But if I execute
this program on my machine, I get
\begin{terminal}
~/.../modernC/code (master % u=) 14:45 <516>$ ./crash
size/alignment: 16/8
offset 16: 0.75 +0.75I 0 +1.125I
offset 8: 0.5 +0I 0.25 +0I
offset 4: Bus error
\end{terminal}

The program crashes with an error indicated as a \jargonindex{bus error},
which is a shortcut for something like ``data bus alignment error.'' The
real problem line is%
\lstextract{26}{26}{code/crash.c}

On the right, we see a pointer cast:
an \code{unsigned} \code{char*} is
converted to a \code{complex} \code{double*}. With the \code{for} loop
around it, this cast is performed for byte offsets \code{offset} from the
beginning of \code{toocomplex}. These are powers of \code{2}: \code{16},
\code{8}, \code{4}, \code{2}, and \code{1}. As you can see in the previous output,
it seems that \code{complex} \code{double} still works well for
alignments of half of its size, but with an alignment of one fourth,
the program crashes.

Some architectures are more tolerant of misalignment than others, and we
might have to force the system to error out on such a condition. We use
the following function at the beginning to force crashing:
\functionDocu{enable_alignment_check}{8}{8}{code/crash.c}

If you are interested in portable code (and if you are still here, you
probably are), early errors in the development phase are really
helpful.\footnote{For the code used inside that function, please
  consult the source code of \texttt{crash.h} to inspect it.} So, consider
crashing a feature. See the blog entry mentioned in \texttt{crash.h} for an interesting discussion of
this topic.


{\lstindexbf
  \label{sec:alignof-para}
  In the previous code example, we also see a new operator, \code{alignof}, that
  provides us with the alignment of a specific type. You will rarely find
  the occasion to use it in real live code.
  Prior to \C{23}, this operator was spelled \code{_Alignof}; if you are
  concerned about legacy code or platforms, you should include
  \Cinclude{stdalign.h} to do the replacement.

  \label{sec:alignas-para}
  Another keyword can be used to force allocation at a specified alignment:
  \code{alignas} (since \C{23}; previously \code{_Alignas}). Its argument can be either
  a type or expression. It can be useful when you know that your platform
  can perform certain operations more efficiently if the data is aligned in
  a certain way.

}

For example, to force alignment of a \code{complex} variable to its size and
not half the size, as we saw earlier, you could use
\begin{shortlisting}
  alignas(sizeof(complex double)) complex double z;
\end{shortlisting}
Or, if you know that your platform has efficient vector instructions for
\code{float[4]} arrays:
\begin{shortlisting}
  alignas(sizeof(float[4])) float fvec[4];
\end{shortlisting}

These operators don't help against the \plainindex[effective type]{effective type} rule
(\mbox{takeaway~\ref{rem:effective-type}}). Even with
\begin{shortlisting}
  alignas(unsigned) unsigned char A[sizeof(unsigned)] = { 9 };
\end{shortlisting}
\noindent
the example at the end of section~\ref{sec:effective-type} remains invalid.

\clearpage
\section*{Summary}
\begin{itemize}
\item The memory and object model have several layers of abstraction:
  physical memory, virtual memory, storage instances, object
  representation, and binary representation.
\item Each object can be seen as an array of \code{unsigned} \tcode{char}.
\item \tcode{union}\/s serve to overlay different object types over the same
  object representation.
\item Memory can be aligned differently according to the need for a
  specific data type. In particular, not all arrays of \code{unsigned} \tcode{char}
  can be used to represent any object type.
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% eval: (reftex-mode)
%%% TeX-master: "modernC.tex"
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
