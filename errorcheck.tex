\clearpage
\subsection{Error checking and cleanup}
\label{sec:goto}

C programs can encounter a lot of error conditions when we test for the
preconditions of an operation or a valid outcome of a C~library
call. Errors can be programming errors, bugs in the compiler or OS
software, hardware errors, resource exhaustion (such as out of memory), or
any malicious combination of these. For a program to be reliable, we have
to detect such error conditions and deal with them gracefully.

As a first example, take the following description of a function
\code{fprintnumbers}, which continues the series of functions that we
discussed in subsection~\ref{sec:text-processing}.
As you can see, this function distinguishes four different error
conditions, indicated by the return of negative constant
values. The macros for these values are generally provided by the platform
in \Cinclude{errno.h}, and all start with the capital letter
\code{E}. Unfortunately, the C standard imposes only \code{EOF}
(which is negative) and \code{EDOM}, \code{EILSEQ}, and \code{ERANGE}, which
are positive.
\functionDocu{fprintnumbers}{80}{83}{code/numberline.c}

Other error values may or may not be provided. Therefore, in the
initial part of our code, we have a sequence of preprocessor statements that
give default values for those that are missing:
\lstpartial{39}{54}{code/numberline.c}

The idea is that we want to be sure to have distinct values
for all of these macros. Now the implementation of the function itself
looks as in the following listing~\ref{lst:fprintnumbers}.
\lstinputlisting[float={t},%
caption={Print an array of numbers},
label={lst:fprintnumbers},%
firstline=175,%
firstnumber=175,%
lastline=203,%
]{code/numberline.c}

Error handling pretty much dominates the coding effort for the
whole function. The first three lines handle errors that occur on entry to
the function and reflect missed preconditions or, in the
language of Annex K (see subsection~\ref{sec:bounds-checking}),
\jargonindex[constraint violation!runtime]{runtime constraint violations}.

Dynamic run-time errors are a bit more difficult to handle. In particular,
some functions in the C library may use the pseudo-variable \code{errno} to
communicate an error condition. If we want to capture and repair all
errors, we have to avoid any change to the global state of the execution,
including to \code{errno}. This is done by saving the current value on
entry to the function and restoring it in case of an error with a call to the
small function \code{error_cleanup}

\lstpartial[float]{150}{153}{code/numberline.c}

The core of the function computes the total number of bytes that should be
printed in a \code{for} loop over the input array. In the body of the loop,
\code{snprintf} with a null pointer and a buffer size of \code{0} are used
to compute the size for each number. Then our function \code{sprintnumbers}
from subsection~\ref{sec:text-processing} is used to produce a big string
that is printed using \code{fputs}.

Observe that there is no error exit after a successful call to
\code{malloc}. If an error is detected on return from the call to
\code{fputs}, the information is stored in the variable \code{tot}, but the
call to \code{free} is not skipped. So, even if such an output error occurs,
no allocated memory is left leaking. Here, taking care of a possible IO
error was relatively simple because the call to \code{fputs} occurred close
to the call to \code{free}.

%\subsection{The use of \code{goto} for cleanup}
\lstset{moreemph={[7]{CLEANUP}}}
The function \code{fprintnumbers_opt} requires more care (see listing~\ref{lst:fprintnumbers_opt}).
%
It tries to optimize the procedure even further by printing the numbers
immediately instead of counting the required bytes first. This may
encounter more error conditions as we go, and we have to take care of them
by guaranteeing to issue a call to \code{free} at the end. The first such
condition is that the buffer we initially allocated is too small. If the
call to \code{realloc} to enlarge it fails, we have to retreat
carefully. The same is true if we encounter the unlikely condition that the
total length of the string exceeds \code{INT_MAX}.
\lstinputlisting[%float={t},%
caption={Printing an array of numbers, optimized version},
label={lst:fprintnumbers_opt},%
firstline=205,%
firstnumber=205,%
lastline=250,%
]{code/numberline.c}

{\lstindexbf In both cases, the function uses \code{goto}} to jump to the
cleanup code that then calls \code{free}. With C, this is a well-established
technique that ensures that the cleanup takes place
and avoids hard-to-read nested \code{if-else}
conditions. The rules for \code{goto} are relatively simple.
\begin{reminder}
  Labels for \code{goto} are visible in the entire function that contains
  them.
\end{reminder}
\begin{reminder}
  \code{goto} can only jump to a label inside the same function.
\end{reminder}
\begin{reminder}
  \code{goto} should not jump over variable initializations.
\end{reminder}


The use of \code{goto} and similar \plainindex[jump]{jumps} in programming languages has been
subject to intensive debate, starting from an article by
\cite{Dijkstra:1968:LEG:362929.362947}. You will still find people
who seriously object to code as it is given here, but let us try to be
pragmatic about that: code with or without \code{goto} can be ugly and hard
to follow. The main idea is to have the ``normal'' \plainindex{control flow} of the
function be mainly undisturbed and to clearly mark changes to the
\plainindex{control flow} that only occur
under exceptional circumstances with a \code{goto} or \code{return}. Later, in
subsection~\ref{sec:long-jumps}, we will see another tool in C that allows even
more drastic changes to the \plainindex{control flow}, \code{setjmp/longjmp},
which enables us to \plainindex[jump!long]{jump} to other positions on the stack of calling
functions.


%%% Local Variables:
%%% mode: latex
%%% eval: (reftex-mode)
%%% TeX-master: "modernC.tex"
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
