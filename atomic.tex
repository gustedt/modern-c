This section covers
\begin{itemize}
\item Understanding the "happened before" relation
\item C library calls that provide synchronization
\item Maintaining sequential consistency
\item Working with other consistency models
\end{itemize}

\bigskip%
\noindent%
We will complete this level with a description of concepts that form an
important part of the C architecture model and are, therefore, a must for
experienced programmers. This last section will focus on increasing
your understanding of how things work, not necessarily improving your
operational skills. Even though we will not go into all the glorious
details,\footnote{We will put aside \code{memory_order_consume} consistency
  and thus the dependency-ordered before relation.} things can get a bit
bumpy; please remain seated and buckle up.

If you review the pictures of \plainindex{control flow} we have seen throughout the
previous sections, you see that the interaction of different parts of a program
execution can get quite complicated. We have different levels of
concurrent access to data:

\medskip%
\begin{itemize}
\item Plain-old straightforward C code is only apparently
  sequential. Visibility of changes is only guaranteed between very
  specific points of the execution, sequence points, for direct data
  dependencies and the completion of function calls. Modern platforms
  take more and more advantage of the provided slack and perform
  unsequenced operations intermixed or in parallel in multiple execution
  pipelines.
\item \plainindex[jump!long]{Long jumps} and \plainindex[signal!handler]{signal handlers} are executed sequentially, but the effects 
  of stores may get lost along the way.
\item Those accesses to atomic objects we have seen so far warrant
  visibility of their changes everywhere and consistently.
\item Threads run concurrently side by side and jeopardize data consistency
  if they don't regulate their shared access to data. In addition to access to
  atomic objects, they can also be synchronized through calls to functions,
  such as \code{thrd_join} or \code{mtx_lock}.
\end{itemize}

\medskip%
\noindent%
Access to memory is not the only thing a program does. In fact, the
abstract state of a program execution consists of the following:
\index{state!abstract}%
\index{abstract state}%

\medskip%
\begin{itemize}
\item \emph{Points of execution} (one per thread)
\item \emph{Intermediate values} (of computed expressions or evaluated
  objects)
\item \emph{Stored values}
\item \emph{Hidden state}
\end{itemize}

\medskip%
\noindent%
Changes to this state are usually described as

\medskip%
\begin{description}
\item[Jumps] Change the point of execution (\plainindex[jump!short]{short jumps}, \plainindex[jump!long]{long jumps},
  and function calls)
\item[Value computation] Changes intermediate values
\item[Side effects] Store values or do IO
\end{description}

\medskip%
\noindent%
Or they can affect hidden state, such as the lock state of a \code{mtx_t} object,
the initialization state of a \code{once_flag} object, and setting or clearing operations
on a \code{atomic_flag} object.

We summarize all these possible changes to the abstract state with the term
\emphindex{effect}.
\begin{reminder}
  Every evaluation has an effect.
\end{reminder}
This is because any evaluation has the concept of a next such evaluation
that will be performed after it. Even an expression like
\begin{shortlisting}
  (void)0;
\end{shortlisting}
which drops the intermediate value and sets the point of execution to the next
statement, and thus the abstract state changes.
\index{state!abstract}%
\index{abstract state}%

In a complicated context, it will be difficult to argue about the actual
abstract state of an execution in a given moment. Generally, the entire
abstract state of a program’s execution is not even observable, and in many
cases, the concept of an overall abstract state is not well
defined because we don't know what \emph{moment} means in
this context. In a multithreaded execution performed on several
physical compute cores, there is no real notion of a reference time between
them. So, generally, C does not even make the assumption that an overall
fine-grained notion of time exists between different threads.

As an analogy, think of two threads, \code{A} and \code{B}, as events that
happen on two different planets orbiting at different speeds
around a star. Times on these planets (threads) are relative, and
synchronization between them takes place only when a signal issued
from one planet (thread) reaches the other. The transmission of the signal
takes time by itself, and when the signal reaches its destination, its source
has moved on. So, the mutual knowledge of the two planets (threads)
is always partial.

\subsection{The ``happened before'' relation}
\label{sec:happ-before-relat}
If we want to argue about a program's execution (its correctness, its
performance, and so on), we need to have enough \emph{partial knowledge} about
the state of all threads and know how to stitch
that partial knowledge together into a coherent view of the whole.

Therefore, we will investigate a relation introduced
by~\cite{Lamport78:time_clock_order}. In C standard terms, it is
the \emphindex{happened before} relation between two evaluations, $E$ and
$F$, denoted by $F \rightarrow E$. We observe this is a property between events
a posteriori. Fully spelled out, it would perhaps be better
called the \emphindex{knowingly happened before} relation instead.

One part of it consists of evaluations in the same thread that are related by the
already-introduced \plainindex{sequenced before} relation:
\begin{reminder}
  If $F$ is sequenced before $E$, then $F \rightarrow E$.
\end{reminder}

To see that, let us revisit listing~\ref{lst:input-thread} from our input
thread. Here, the assignment to \code{command[0]} is sequenced before the
\code{switch} statement. Therefore, we are sure that all cases in the
\code{switch} statement are executed \emph{after} the assignment or at
least that they will be \emph{perceived} as happening later. For example,
when passing \code{command} to the nested function calls below
\code{ungetc}, we are sure it will provide the modified value.
All of this can be deduced from C's grammar.

Between threads, the ordering of events is provided by
\emphindex{synchronization}. There are two types of synchronization: the
first is implied by operations on atomics, and the second, by certain C library
calls. Let us first look into the case of atomics. An atomic object can be
used to synchronize two threads if one thread writes a value and another
thread reads the value that was written. Operations on atomics are guaranteed
to be locally consistent.
\begin{reminder}
  The set of modifications of an atomic object \code{X} is performed in an
  order consistent with the sequenced before relation of any
  thread that deals with \code{X}.
\end{reminder}

This sequence is called the \emphindex{modification order} of
\code{X}. Consider an atomic variable \code{x},

\begin{shortlisting}
  _Atomic(unsigned) x = 11;
\end{shortlisting}
a thread \code{A} that performs the instructions
\begin{shortlisting}
  ++x;
  x = 31;
  …
  unsigned y = x
  x = 0;
\end{shortlisting}
and a thread \code{B} that mingles with the same variable \code{x}
\begin{shortlisting}
  x--;
  x = 5;
\end{shortlisting}

\begin{figure}%[h]
  \begin{center}
    \hspace*{4em}\input{fig/atomic-mod-order.pdf_t}
    \caption{
      Two threads that synchronize via an atomic. The circles present the
      modifications of object \code{x}. The bars below the
      threads represent information about the state of $A$, and those
      above represent information about the state of $B$.
      \label{fig:mod-order}
    }
  \end{center}
\end{figure}
A possible modification order for \code{x} is shown in
figure~\ref{fig:mod-order}. Here, we have six modifications to \code{x}:
the initialization (value \code{11}), one increment, one decrement, and
three assignments. The C standard guarantees that each of the two threads,
\code{A} and \code{B}, perceives all changes to \code{x} in an order
consistent with this modification order.

In the example in figure~\ref{fig:mod-order}, we have only two synchronizations. First,
thread \code{B} synchronizes with \code{A} at the end of its \code{--x}
operation because here it has read (and modified) the value \code{31} that
\code{A} wrote. The second synchronization occurs when \code{A} reads
the value \code{5} that \code{B} wrote and stores it into \code{y}.

As another example, let us investigate the interplay between the input
thread (listing~\ref{lst:input-thread}) and the account thread
(listing~\ref{lst:account-thread}). Both read and modify the field
\code{finished} in different places. For the simplicity of the argument,
let us assume that there is no other place than in these two functions
where \code{finished} is modified.

The two threads will only synchronize via this atomic object if either
modifies it---that is, writes the value \code{true} into it. This can happen
under two circumstances:

\medskip%
\begin{itemize}
\item The input thread encounters an end-of-file condition, either when
  \code{feof(stdin)} returns \code{true} or if the case \code{EOF} is
  encountered. In both cases, the \code{do} loop terminates, and the code after
  the label \code{FINISH} is executed.
\item The account thread detects that the number of permitted repetitions is
  exceeded and sets \code{finished} to \code{true}.
\end{itemize}

\medskip%
\noindent%
These events are not exclusive, but using an atomic object guarantees that
one of the two threads will succeed first in writing to \code{finished}:

\medskip%
\begin{itemize}
\item If the input thread writes first, the account thread may read the
  modified value of \code{finished} in the evaluations of one of its
  \code{while} loops. This read synchronizes; that is, the write event in
  the input thread is known to have happened before such a read. Any
  modifications the input thread made before the write
  operation is now visible to the account thread.
\item If the account thread writes first, the input thread may read the
  modified value in the \code{while} of its \code{do} loop. Again, this
  read synchronizes with the write and establishes a ``happened before''
  relation, and all modifications made by the account thread are
  visible to the input thread.
\end{itemize}

\medskip%
\noindent%
Observe that these synchronizations are oriented:
each synchronization between threads has a ``writer'' and a ``reader''
side. We attach two abstract properties to operations on atomics
and to certain C library calls called \emphindex{release}
semantics (on the writer side), \emph{acquire} semantics (for a reader), and
\emph{acquire-release} semantics (for a reader-writer). C library calls
with such synchronization properties will be discussed a bit later.

All operations on atomics we have seen so far and modify the
object are required to have release semantics, and all that read must have
acquire semantics. Later, we will see other atomic operations that have
relaxed properties.
\begin{reminder}
  An acquire operation $E$ in a thread $T_E$ synchronizes with a release
  operation $F$ in another thread $T_F$ if $E$ reads the value that $F$ has
  written.
\end{reminder}
The idea of the special construction with acquire and release semantics is
to force the visibility of effects across such operations. We say that an
effect $X$ is \emphindex[effect!visible]{visible} at evaluation $E$ if we
can consistently replace $E$ with any appropriate read operation or function
call that uses the state affected by $X$. For example, in figure~\ref{fig:mod-order},
the effects \code{A} produces before its \code{x = 31} operation are
symbolized by the bar below the thread. They are visible to \code{B} once
\code{B} has completed the \code{--x} operation.
\begin{reminder}
  If $F$ synchronizes with $E$, all effects $X$ that happened before
  \code{F} must be visible at all evaluations \code{G} that happen after
  \code{E}.
\end{reminder}

As we saw in the example, there are atomic operations that
can read \emph{and} write atomically in one step. These are called
\emphindex{read-modify-write} operations:

\medskip%
\begin{itemize}
\item Calls to \code{atomic_exchange} and
  \code{atomic_compare_exchange_weak} for any\linebreak[3]
  \code{_Atomic} objects
\item Compound assignments or their functional equivalents; increment and
  decrement operators for any \code{_Atomic} objects of arithmetic type
\item Calls to \code{atomic_flag_test_and_set} for \code{atomic_flag}
\end{itemize}

\medskip%
\noindent%
Such an operation can synchronize on the read side with one
thread and on the write side with others. All such read-modify-write
operations that we have seen so far have both acquire and release
semantics.

The \plainindex{happened before} relation is the transitive closure of the
combination of the \plainindex{sequenced before} and
\plainindex{synchronizes-with} relations. We say that $F$ knowingly
happened before $E$, if there are $n$ and
$E_0 = F, E_1, ... , E_{n-1}, E_n = E$ such that $E_i$ is sequenced before
$E_{i+1}$ or synchronizes with it for all $0\leq i<n$.
\begin{reminder}
  We only can conclude that one evaluation happened before another if we
  have a sequenced chain of synchronizations that links them.
\end{reminder}
Observe that this \plainindex{happened before} relation is a combination of
very different concepts. The \plainindex{sequenced before} relation can in
many places be deduced from syntax, in particular, if two statements are
members of the same basic block. Synchronization is different: besides the
two exceptions of thread startup and end, it is deduced through a data
dependency on a specific object, such as an atomic or a mutex.

The desired result of all this is that effects in one thread become
visible in another.
\begin{reminder}
  If an evaluation $F$ happened before $E$, all effects that are known to
  have happened before $F$ are also known to have happened before $E$.
\end{reminder}

\subsection{C library calls that provide synchronization}
\label{sec:synchr-c-libr}

C library functions with synchronizing properties come in pairs: a
releasing side and an acquiring side. They are summarized in
table~\ref{tab:sync-pairs}.
\begin{table}[h]
  \begin{center}
    \caption{C library functions that form synchronization pairs}
    \label{tab:sync-pairs}
    \begin{tabular}{p{0.5\textwidth}|p{0.5\textwidth}}
      Release & Acquire \\
      \hline
      \code{thrd_create(.., f, x)} & Entry to \code{f(x)} \\
      \hline
      \code{thrd_exit} by thread \code{id}  or \code{return} from \code{f}
              & Start of \code{tss_t} destructors for \code{id}\\
      \hline
      End of \code{tss_t} destructors for \code{id}
              & \code{thrd_join(id)} or \code{atexit/at_quick_exit} handlers\\
      \hline
      \code{call_once(&obj, g)}, first call
                        & \code{call_once(&obj, h)}, all subsequent calls\\
      \hline
      Mutex release            & Mutex acquisition \\
    \end{tabular}
  \end{center}
\end{table}

For the first three entries, we know which events synchronize
with which; namely, the synchronization is mainly limited to effects done by
thread \code{id}. In particular, by transitivity,
\code{thrd_exit} or \code{return} always synchronize with \code{thrd_join}
for the corresponding thread \code{id}.

These synchronization features of \code{thrd_create} and \code{thrd_join}
allow us to draw the lines in figure~\ref{fig:B9}. Here, we do not know
about any timing of events between the threads we launched, but within
\code{main}, we know that the order in which we created the threads and the
order in which we joined them is exactly as shown. We also know that all
effects of any of these threads on data objects are visible to \code{main}
after we join the last thread: the account thread.

If we detach our threads and don't use \code{thrd_join}, synchronization can
only take place between the end of a thread and the start of an
\code{atexit} or \code{at_quick_exit} handler.

The other library functions are a bit more complicated. For the
initialization utility \code{call_once}, the return from the very first
call \code{call_once(&obj, g)} is special: it is the only call that
effectively makes a call into the callback function \code{g}. This first
call is a release operation for all subsequent calls with the same object
\code{obj}. These all are then acquire operations on this object. This
ensures that all write operations performed during the call to
\code{g()} are known to happen before any other call with
\code{obj} and are effectively visible in the corresponding thread.

For our example in subsection~\ref{sec:race-free-init}, this means the
function \code{errlog_fopen} is executed exactly once, and all other
threads that might execute the \code{call_once} line will synchronize with
that first call. So when any of the threads return from the call, they know
that the call has been performed (either by themselves or by another thread
that was faster) and that all effects, such as computing the file name
and opening the stream, are visible now. Thus, all threads that executed
the call may use \code{errlog} and can be sure it is properly
initialized.

For a mutex, a release operation can be a call to a mutex function,
\code{mtx_unlock}, or the entry into the wait functions for condition
variables, \code{cnd_wait} and \code{cnd_timedwait}. An acquire operation on
a mutex is the successful acquisition of the mutex via any of the three
mutex calls (\code{mtx_lock}, \code{mtx_trylock}, and \code{mtx_timedlock})
or the return from the wait function (\code{cnd_wait} or
\code{cnd_timedwait}).
\begin{reminder}
  Critical sections protected by the same mutex occur
  sequentially.
\end{reminder}

Our input and accounting threads from the example
(listings~\ref{lst:input-thread} and~\ref{lst:account-thread}) access the
same mutex \code{L->mtx}. In the first, it is used to protect the birth of a
new set of cells if the user types a \code{' '}, \code{'b'}, or
\code{'B'}. In the second, the entire secondary block of the \emph{while} loop is
protected by the mutex.

Figure~\ref{fig:mutex-mod-order} schematizes a sequence of three critical
sections protected by the mutex. The synchronization between the
unlock operations (release) and the return from the lock operation
(acquire) synchronizes the two threads. This guarantees that the changes
applied to \code{*L} by the account thread in the first call to
\code{life_account} are visible in the input thread when it calls
\code{life_birth9}. Equally, the second call to \code{life_account} sees
all changes to \code{*L} that occur during the call to \code{life_birth9}.

\begin{figure}%[h]
  \begin{center}
    \hspace*{-3em}\input{fig/mutex-mod-order.pdf_t}
    \caption{
      Two threads with three critical sections that synchronize via a
      mutex. The circles present the modifications of object \code{mtx}.
      \label{fig:mutex-mod-order}
    }
  \end{center}
\end{figure}

\begin{reminder}
  In a critical section protected by the mutex \code{mut}, all effects
  of previous critical sections protected by \code{mut} are visible.
\end{reminder}
One of these known effects is always the advancement of the point of
execution. In particular, on return from \code{mtx_unlock}, the execution
point is outside the critical section, and this effect is known to the next
thread that newly acquires the lock.

The wait functions for condition variables differ from acquire-release
semantics; in fact, they work exactly the other way around.
\begin{reminder}
  \code{cnd_wait} and \code{cnd_timedwait} have release-acquire semantics
  for the mutex.
\end{reminder}
That is, before suspending the calling thread, they perform a release
operation and then, when returning, an acquire operation. The other
peculiarity is that the synchronization goes through the mutex,
not through the condition variable itself.
\begin{reminder}
  Calls to \code{cnd_signal} and \code{cnd_broadcast} synchronize via the mutex.
\end{reminder}
The signaling thread will
not necessarily synchronize with the waiting thread if it does not place a
call to \code{cnd_signal} or \code{cnd_broadcast} into a critical section
protected by the same mutex as the waiter. In particular,
non-atomic modifications of objects that constitute the \emph{condition
  expression} may not become visible to a thread woken up by a
signal if the modification is not protected by the mutex. There is a simple
rule of thumb to ensure synchronization:
\begin{reminder}
  Calls to \code{cnd_signal} and \code{cnd_broadcast} should occur
  inside a critical section protected by the same mutex as the
  waiters.
\end{reminder}
This is what we saw around line~\ref{lab:birth9} in
listing~\ref{lst:input-thread}. Here, the function \code{life_birth} modifies
larger, non-atomic parts of the shared object \code{*L}, so we must make sure these
modifications are properly visible to all other threads that work with
\code{*L}.

Line~\ref{lab:signal} shows a use of \code{cnd_signal} that is not
protected by the mutex. This is only possible here because all data
modified in the other \code{switch} cases is atomic. Thus, other threads
that read this data, such as \code{L->frames}, can synchronize
through these atomics and do not rely on acquiring the mutex. Be careful
if you use conditional variables like that.

\subsection{Sequential consistency}
\label{sec:sequ-consistency}

The data consistency for the previously described atomic objects, guaranteed by the
\emph{happened before} relation, is called \emphindex{acquire-release
  consistency}. Whereas the C library calls we have seen always
synchronize with that kind of consistency, no more and no less, accesses
to atomics can be specified with different consistency models.

As you remember, all atomic objects have a \emph{modification order} that
is consistent with all \plainindex{sequenced before} relations that see these
modifications \emph{on the same} object. \emphindex[sequential
consistency]{Sequential consistency} has even more requirements than
that; see figure~\ref{fig:seq-cst}.
%
Figure~\ref{fig:seq-cst} illustrates the common timeline of all sequentially consistent
operations on top. Even if these operations are performed on different
processors and the atomic objects are realized in different memory banks,
the platform has to ensure that all threads perceive all these operations
as being consistent with this one global linearization.
\begin{reminder}
  All atomic operations with sequential consistency occur in one global
  modification order, regardless of the atomic object they are
  applied to.
\end{reminder}
\begin{figure}%[h]
  \begin{center}
    \hspace*{3em}\input{fig/seq-cst.pdf_t}
    \caption{
      Sequential consistency for three different atomic objects
      \label{fig:seq-cst}
    }
  \end{center}
\end{figure}

So, sequential consistency is a very strong requirement. Not only that, it
enforces acquire-release semantics (a causal partial ordering
between events), but it extends this partial ordering to a total ordering.
If you are interested in parallelizing the execution of your program,
sequential consistency may not be the right choice because it may force
sequential execution of the atomic accesses.

The standard provides the following functional interfaces for atomic types.
They should conform to the description given by their name and also
perform synchronization:
\begin{shortlisting}
void atomic_store(A volatile* obj, C des);
C atomic_load(A volatile* obj);
C atomic_exchange(A volatile* obj, C des);
bool atomic_compare_exchange_strong(A volatile* obj, C *expe, C des);
bool atomic_compare_exchange_weak(A volatile* obj, C *expe, C des);
C atomic_fetch_add(A volatile* obj, M operand);
C atomic_fetch_sub(A volatile* obj, M operand);
C atomic_fetch_and(A volatile* obj, M operand);
C atomic_fetch_or(A volatile* obj, M operand);
C atomic_fetch_xor(A volatile* obj, M operand);
bool atomic_flag_test_and_set(atomic_flag volatile* obj);
void atomic_flag_clear(atomic_flag volatile* obj);
\end{shortlisting}
Here \code{C} is any appropriate data type, \code{A} is the corresponding
atomic type, and \code{M} is a type that is compatible with the arithmetic
of \code{C}. As the names suggest, for the fetch and operator interfaces,
the call returns the value that \code{*obj} had before the modification of
the object. So, these interfaces are not equivalent to the
corresponding compound assignment operator (\code{+=}), since that would return the
result \emph{after} the modification.

All these functional interfaces provide \emphindex{sequential consistency}.
\begin{reminder}
  All operators and functional interfaces on atomics that don't specify
  otherwise have sequential consistency.
\end{reminder}

Observe also that the functional interfaces differ from the operator forms
because their arguments are \code{volatile} qualified.

There is another function call for atomic objects that, in contrast to the
function we have seen until now, does not
imply synchronization:
\begin{shortlisting}
void atomic_init(A volatile* obj, C des);
\end{shortlisting}
Its effect is the same as a call to \code{atomic_store} or an assignment
operation, but concurrent calls from different threads can produce a
race. View \code{atomic_init} as a cheap form of assignment.


\subsection{Other consistency models}
\label{sec:other-cons-models}
A different consistency model can be requested with a complementary set of
functional interfaces. For example, an equivalent to the postfix \code{++} operator
with just acquire-release consistency could be specified with
\begin{shortlisting}
  _Atomic(unsigned) at = 67;
  ...
  if (atomic_fetch_add_explicit(&at, 1, memory_order_acq_rel)) {
    ...
  }
\end{shortlisting}

\begin{reminder}
  Synchronizing functional interfaces for atomic objects with
  \code{_explicit} appended allow us to specify their consistency model.
\end{reminder}

These interfaces accept additional arguments in the form of symbolic
constants of type \icode{memory_order} that specify the memory semantics of
the operation:

\clearpage%
\begin{itemize}
\item \icode{memory_order_seq_cst} requests sequential consistency. Using
  this is equivalent to the forms without \code{_explicit}.
\item \icode{memory_order_acq_rel} is for an operation with
  acquire-release consistency. Typically, for general atomic types, you'd use
  it for a read-modify-write operation, such as\\
  \code{atomic_fetch_add} or \code{atomic_compare_exchange_weak},
  or for the type \code{atomic_flag} with
  \code{atomic_flag_test_and_set}.
\item \icode{memory_order_release} is for an operation with only release
  semantics. Typically, this would be \code{atomic_store} or
  \code{atomic_flag_clear}.
\item \icode{memory_order_acquire} is for an operation that only has acquire
  semantics. Typically, this would be \code{atomic_load}.
\item \icode{memory_order_consume} is for an operation that has a weaker
  form of causal dependency
  than acquire consistency. Typically, this would
  also be \code{atomic_load}.
\item \icode{memory_order_relaxed} is for an operation that adds no
  synchronization requirements. The only guarantee for
  such an operation is that it is indivisible. A typical use case
  is a performance counter used by different threads,
  but for which we are only interested in a final accumulated count.
\end{itemize}

\medskip%
\noindent%
The consistency models can be compared with respect to the restrictions
they impose on the platform. Figure~\ref{fig:memory_order} shows the
implication order of the \code{memory_order} models.

\begin{figure}[h]
  \begin{center}
    \input{fig/memory_order.pdf_t}
    \caption{Hierarchy of consistency models, from least to most constraining}
    \label{fig:memory_order}
  \end{center}
\end{figure}

Whereas \code{memory_order_seq_cst} and \code{memory_order_relaxed} are
admissible for all operations, there are some restrictions for other
\tcode{memory_order}\/s. Operations that can only occur on one side of a
synchronization can only specify an order for that side:

\medskip%
\begin{itemize}
\item Two operations that only store (\code{atomic_store} and
  \code{atomic_flag_clear}) may not specify acquire semantics.
\item Three operations that only perform a load and may not specify release
  or consume semantics; these are \code{atomic_load} (for the single
  \code{memory_order} argument) and the weak and strong versions of
  compare-exchange operations in case of failure.
\end{itemize}

\medskip%
\noindent%
Thus, the latter needs two \code{memory_order} arguments for their
\code{_explicit} form, such that they can distinguish the requirements for
the success and failure cases:

\clearpage
\begin{shortlisting}
bool
atomic_compare_exchange_strong_explicit(A volatile* obj, C *expe, C des,
                                        memory_order success,
                                        memory_order failure);
bool
atomic_compare_exchange_weak_explicit(A volatile* obj, C *expe, C des,
                                      memory_order success,
                                      memory_order failure);
\end{shortlisting}
Here, the \code{success} consistency must be at least as strong as the
\code{failure} consistency; see figure~\ref{fig:memory_order}.

Up to now, we have implicitly assumed that the acquire and release sides of a
synchronization are symmetric, but they aren't. Whereas there is always
just one writer of a modification, there can be several readers. Because moving
new data to several processors or cores is expensive, some platforms allow us
to avoid the propagation of all visible effects that happened before an
atomic operation to all threads that read the new value. C's
\emphindex{consume consistency} is designed to map this behavior. We will
not go into the details of this model, and you should use it only when you
are certain that some effects prior to an atomic read will not affect the
reading thread.

\clearpage
\section*{Summary}
\begin{itemize}
\item The "happened before" relation is the only possible way to reason about
  timing between different threads. It is only established through
  synchronization that uses either atomic objects or very specific C
  library functions.
\item Sequential consistency is the default consistency model for atomics
  but not for other C library functions. It additionally assumes that all
  corresponding synchronization events are totally ordered. This
  assumption can be expensive.
\item Explicitly using acquire-release consistency can lead to more
  efficient code, but it needs a careful design to supply the correct
  arguments to the atomic functions with a \code{_explicit} suffix.
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "modernC.tex"
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
