\subsection{Using restrict qualifiers}
\label{sec:avoid-alias-restrict}

{\lstindexbf%
We have seen many examples of C library functions that use the keyword
\code{restrict} to qualify pointers, and we also have used this
qualification for our own functions. The basic idea of \code{restrict} is
relatively simple: it tells the compiler that the pointer in question is
the only access to the object it points to. Thus, the compiler can make the
assumption that changes to the object can only occur through that same
pointer, and the object cannot change inadvertently. In other words, with
\code{restrict}, we are telling the compiler that the object does not alias
any other object the compiler handles in this part of the code.


\begin{reminder}
  A \code{restrict}-qualified pointer has to provide exclusive access.
\end{reminder}

As is often the case in C, such a declaration places the burden of verifying this
property on the caller.

\begin{reminder}
  A \code{restrict}-qualification constrains the caller of a function.
\end{reminder}

Consider, for example, the differences between \code{memcpy} and \code{memmove}:

\begin{lstlisting}
void* memcpy(void*restrict s1, void const*restrict s2, size_t n);
void* memmove(void* s1, const void* s2, size_t n);
\end{lstlisting}

For \code{memcpy}, both pointers are \code{restrict}-qualified. So, for the
execution of this function, the access through both pointers has to be
exclusive; otherwise, the execution fails. Also, \code{s1} and \code{s2} must have different
values, and neither can provide access to parts of the object of the
other. In other words, the two objects that \code{memcpy} ``sees'' through
the two pointers must not overlap. Assuming this can help to optimize the
function.

In contrast, \code{memmove} does not make such an assumption. So,
\code{s1} and \code{s2} may be equal, or the objects may overlap. The function must be
able to cope with that situation. Therefore, it might be less efficient, but
it is more general.

\index{aliasing}%
We saw in subsection~\ref{sec:memory-state} that it might be
important for the compiler to decide whether two pointers may, in fact, point to
the same object (aliasing). Pointers to different base types are
not supposed to alias unless one of them is a character type.%
} %
So, both parameters of \code{fputs} are declared with \code{restrict}:
\begin{lstlisting}
int fputs(const char *restrict s, FILE *restrict stream);
\end{lstlisting}
However, it seems very unlikely that anyone would call \code{fputs}
with the same pointer value for both parameters.

This specification is more important for functions like \code{printf} and friends:
\begin{lstlisting}
int printf(const char *restrict format, ...);
int fprintf(FILE *restrict stream, const char *restrict format, ...);
\end{lstlisting}
The \code{format} parameter shouldn't alias any of the
arguments that might be passed to the \code{...} part. For example,
reaching the following
code, the execution fails:
\index{failure!program}%
\index{program failure}%
\index{behavior!undefined}%
\index{undefined behavior}%
\begin{lstlisting}
  char const* format = "format printing itself: %s\n";
  printf(format, format);   // Restrict violation
\end{lstlisting}
This example will probably still do what you think it does. If you
abuse the \code{stream} parameter, your program might explode:
\begin{lstlisting}
  char const* format = "First two bytes in stdin object: %.2s\n";
  char const* bytes = (char*)stdin; // Legal cast to char
  fprintf(stdin, format, bytes);    // Restrict violation
\end{lstlisting}

Sure, code like this is not very likely to occur in real life. But keep in
mind that character types have special rules concerning aliasing, and
therefore all string-processing functions may be subject to missed
optimization. You could add \code{restrict} qualifications in many places
where string parameters are involved and that you know are
accessed exclusively through the pointer in question.


%%% Local Variables:
%%% mode: latex
%%% eval: (reftex-mode)
%%% TeX-master: "modernC.tex"
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
