From: Programming 2019 <programming2019@easychair.org>
To: Jens Gustedt <jens.gustedt@inria.fr>
Subject: Programming 2019 notification for paper 3
Sender: programming2019@easychair.org

Dear Jens, 

thanks again for submitting to the <Programming> journal. 

The reviewers found the idea of a C module system very interesting, 
and believe this is a fruitful avenue of research. However, they think that the changes 
needed to make this a solid paper require significant work which can only by done by one of the next submission rounds. 
For this reason, the paper will not be published in the next Issue of the journal. 

Given the positive feedback of the reviewers, however, I definitely encourage you to 
revise the paper according to the reviews and resubmit. 
If you do that, the paper will be assigned to the same reviewers, who will be available 
to read it and evaluate it again. I believe that these are the main points to consider: 

- Contextualize this contribution in the large body of knowledge about module systems. Highlighting the novel contribution and the specific challenges in the case of C. See e.g., 

Xavier Leroy. 2000. A modular module system. J. Funct. Program. 10, 3 (May 2000), 269-303. DOI=http://dx.doi.org/10.1017/S0956796800003683

and references therein. There are already proposals for
module systems for system software that need to be taken into account:

Alastair Reid, Matthew Flatt, Leigh Stoller, Jay Lepreau, and Eric Eide. 2000. Knit: component composition for systems software. In Proceedings of the 4th conference on Symposium on Operating System Design & Implementation - Volume 4 (OSDI'00), Vol. 4. USENIX Association, Berkeley, CA, USA, , pages.

- Extend the work with some form of evaluation. As it is now, the paper has a reather practical flavor - which is definitely in scope for <Programming>. It is based on a simple mechanism that has the potential to improve a lot of existing software. In this mechanism effective? What is the experience of the author in adopting such mechanism?

If you decide to proceed in this direction, I suggest that you also include an answer letter with the changes made to the paper punctually referring to the current reviews. 


I remain at your disposal for any clarification. 

Best Regards 
Guido


----------------------- REVIEW 1 ---------------------
PAPER: 3
TITLE: Modular C
AUTHORS: Jens Gustedt

Overall evaluation: 1 (weak accept, needs some revision)

----------- Review -----------
Paper summary:
This paper proposes an extension of C, called Modular C. This extensions adds a number of compiler directives to C in order to enable modular programming in C. The paper shows that these compiler directives can be translated into valid common C. The translation procedure is formalised and proven to be correct.

In favour:
The idea in the paper is well presented and a formalisation is given that proves its correctness.

Against:
The idea presented in the paper is properly motivated from the perspective of a C programmer. However, the main issue I have with this paper is that it is not properly situated within the larger body of related work. This makes it very hard to gauge where exactly this paper makes a contribution.

Does this paper advance our understanding of how to design good module systems? If so, how does this extension compare to other module systems? Both for C and for other low-level programming languages?

Does this paper contribute to the field of source code translation? If so, do other module systems exist that are integrated with the language using source code translation? How does the approach compare to those?

The only relevant discussion I've found in the paper is about reference [2]. And the paper remains vague in its comparison to that work.

Minor remarks:
The English in the paper is rather sloppy. I'm not a native speaker myself, so I can tolerate a few mistakes. However, in this case it made the content hard to process at certain times.
Page 3: "that have been made, there." => "that have been made there."
Page 3: "only reserve if the prefix is followed by uppercase, add a suffix such as _t" => "are only reserved if their prefix is followed by an uppercase letter or a suffix such as _t"
Page 4: "The may be split into two different categories" => "They can be split into two different categories"
page 4: "by design Go is a rupture" => I don't think rupture is the correct noun to be used here
page 4: "The present proposal" => "The presented proposal"
page 4: "the one's mentioned" => "the ones mentioned"
page 5: "importer nor interfere" => "importer, nor interfere"
page 5: "identifiers as they are necessary for usual naming conventions" => "identifiers that are necessary when using naming conventions"
page 5: "This is e.g useful" => "This is e.g. useful"
page 7: "main looses its" => "main loses its"
page 7: "following we go a bit into details how" => "following paragraph we go a bit into detail how"
page 8: "we may easily" => "we can easily"
page 8: "abbreviations. It can be applied" => "abbreviations. They can be applied"
page 9: "e.g" => "e.g."
page 9 "all our explicit or implicit interface inclusion ensures that we only have what they call horizontal dependency" => "all our explicit or implicit interface inclusions ensure that we only have what they call a horizontal dependency"
page 9: "that had been observed" => "that has been observed"
page 11: "guaranty" => "guarantee"
page 12: "the later implies" => "the latter implies"
page 13: "Here it the function" => "Here the function"
page 13: Listing 3 is referred to before Listing 2
page 16: H(P) is used before it is defined
page 19: "it remains to prove the statement" => "it remains to be proven that the statement"
page 20: "invested some month" => "invested some months" or "invested a month"
page 21: "all these replacement so far" => "all these replacements so far"
page 22:

"This is done without introducing new syntactical constructs to the core language, and allows for a grouping of features that is more flexible than classes in traditional object-oriented languages."

Where is this statement argued in the paper? Either this is properly motivated or you remove it altogether from your conclusion.

page 22:

"Other features are added to Modular C that fall out from the global approach almost effortlessly, but can nonetheless be helpful tools for future software projects."

I have some idea of what you are saying here, but I don't fully understand this line. Are you saying there are more features of "Modular C" than just the module system?


----------------------- REVIEW 2 ---------------------
PAPER: 3
TITLE: Modular C
AUTHORS: Jens Gustedt

Overall evaluation: -1 (weak reject, some problems)

----------- Review -----------
I very much like many elements of this proposal for Modular C---it is clean (just a few directives and a naming convention), based on a simple new feature (composed identifiers), and potentially an effective means of providing encapsulation.

I couldn't help thinking about long standing means of virtualizing interfaces from 30 years ago, such as VFS (albeit in OS code the relies heavily on macros and compiler directives!), and wondering about how this approach would change systems written in C that I'm familiar with today... I'm thinking mostly of systems code (where naming conventions have been around forever), where the dynamic module startup would be incredibly useful.

Even though I'm a little on the fence about some of the features (will "snippets" scale well in terms of comprehension?), I'm highly optimistic about the work in general.  My sense is that keeping the change to C minimal is critical for adoption in the systems community.  

However, for me, the sketch of how to transform existing projects fell short of what I was hoping for.  The author did not provide me with enough a concrete assessment to determine if the process and tradeoffs are reasonable and practical at scale.  My understanding is this work has done for several projects, but just not reported in this paper (nor in what I could find online).

Pros:
- motivation very nicely written 
- simple and clean approach, several interesting elements  
- formalism and proof of correctness
- the structured approach to the C library

Cons:
- I very much wanted to see the practical ramifications from the projects the approach has been applied to
- though the author reports "reasonable effort", metrics showing how many/what type of changes were required with small/med/large projects would be the game changer in the paper for me
- this could help me with questions like: what programs aren't stable? what are the tradeoffs?  where is the threshold for too many macros? how are the interfaces better (both ways)?


----------------------- REVIEW 3 ---------------------
PAPER: 3
TITLE: Modular C
AUTHORS: Jens Gustedt

Overall evaluation: -2 (reject, serious problems)

----------- Review -----------
# Comments

This paper proposes an extension to the C standard called "Modular C",
which provides a set of directives for modules. Modules can import
other modules through the import directive if the relation is
acyclic. Unlike C's include, import ensures complete encapsulation
among modules. It also provides parameterized code injection through
snippets, which equip slots to be filled when they are imported and
works as templates in C++ and so-called X macros. The paper gives a
translation scheme from Modular C to C and shows several properties of
modules and the translation scheme. A modular C compiler is
implemented using POSIX and Gnu tools and the translated code compiles
with gcc and clang.

The subreviewer completely agree that C needs a module system instead
of relying on careful naming convention and think that the ideas given
in the paper is useful practical. The subreviewer, however, cannot
recommend accepting the paper because of the following problems.

## Directives and their semantics are not clear

It is hard to get the set of directives Modular C provides and
understand the semantics of them. A table of directives may be helpful
for the first half of the problem. The second half of the problem is
more significant. For example, I am not sure whether it is okay to
write

  # pragma CMOD import proj::structs

instead of

  # pragma CMOD import ..::element

in p.6. I guess that it is natural that the target of an "import" is a
module and importing a module make the names defined in the module
available, but the paper explains only the case that each name in a
module is imported. Does Modular C provide a way to import all the
names that are defined in a module?

Another example is:

  # pragma CMOD module head = proj::structs::list

I guess that it specifies proj::structs::list as the name of the
module and head as its abbreviation. Section 2.3, however, says that
head is a composed identifier of special interest and the two examples
in the section seem that the above pragma should be understood as "if
proj::structs::list is imported, then the struct head is
imported". What is correct?

## Rule 2.6

The paper explains as Rule 2.6 that identifiers without external
linkage are exported iff they are in a declaration section. I cannot
understand why identifiers without external linkage are exported. Do
you mean that identifiers with external linkage are exported?

## Conforming

I cannot find any definitions of "conforming module" and
"conforming C source". What are they? Without them, I cannot imagine
how to prove the lemmas and theorem.

## Relation with other module systems

There are lots of studies on module systems but they are rarely
mentioned. For example, why it is not suitable for C to adopt a module
system in ML[1]? 

[1] XAVIER LEROY, "A modular module system", Journal of Functional
Programming, Volume 10, Issue 3, May 2000, pp. 269-303


# Minor comments

* p2. programmable interface -> programming interface
* p4. The may be -> This may be?
* p7. that is be visible as -> that is visible as


----------------------- REVIEW 4 ---------------------
PAPER: 3
TITLE: Modular C
AUTHORS: Jens Gustedt

Overall evaluation: 2 (accept, minor revision)

----------- Review -----------
Summary:

  This paper describes the language Modular C, which is an extension of C11
  that adds composite identifiers and a handful of directives to the language,
  thus replacing the textual `#include` mechanism by an object file based
  `import` mechanism with encapsulation based on name mangling; it also adds
  support for 'snippets' which are a kind of macros that enable code reuse;
  finally, Modular C supports module initialization and finalization support,
  with import based ordering guarantees. The approach is presented
  conceptually and in terms of the intended advantages, followed by a rather
  detailed listing of contributions and an example. Next, the language is
  described in more detail, with examples and rules as the basic structural
  elements. Next, a more formal section describes such notions as stable
  programs and modules, and consistent projects; it then proceeds to give
  several lemmas and theorems about the properties of the translation to C,
  including snippets and startup/finalization. Finally, migration of C code to
  Modular C is discussed.

Evaluation:

  The main strength of this paper is that it is convincingly connected to
  real world software, full-fledged languages, and actual development work.

  The main contribution is the improvements in encapsulation (which would
  make it easier to reason about the effect of combining many modules into a
  complex system). The support for snippets (macros) is described in a rather
  incomplete manner (e.g., there is very little information about how 'fill'
  and 'defill' works, and several other aspects of snippets are basically just
  given as hints, and it's up to the reader to guesstimate how it works).

  It could be claimed that several other properties of languages with a
  shorter history than C are important contributions as well. E.g., the fact
  that the amount of redundancy in declarations is reduced significantly
  (compared to C, where many function signatures are duplicated in a *.h
  file for #inclusion and in a *.c file as part of the definition of the
  function), or the fact that `foreach` and `do` directives can be used to
  make macros more powerful than in C. However, I'd put these benefits in the
  second row, compared to the improved encapsulation.

  One design choice which seems to be potentially costly is that modules
  cannot import each other in a cyclic manner. This may sound nice and tidy at
  first (it's very easy to find advice on StackOverflow etc. to avoid these
  cycles), but a lot of experience seems to indicate that they are hard to
  avoid completely. For some initial studies of problems of this nature, check
  out, e.g., this thesis:
  https://mro.massey.ac.nz/bitstream/handle/10179/5465/02_whole.pdf
  It would be interesting to hear about ideas that would allow Modular C to
  handle cyclic imports, maybe just in some "suffiently small" clusters.

  The presentation is very practical (as opposed to one that uses a lot of
  subtle mathematical abstractions), and this is typically good for the
  readability of the paper. However, it also causes the paper to be incomplete
  or imprecise in a number of ways. I already mentioned that the 'fill' and
  'defill' directives are described in a very brief and imprecise manner (a
  wild guess is that it is a snippet parameter mechanism), but there are many
  other examples. The detailed comments below indicate several such problems
  with the precision or completeness. Another example is that the word 'Rule'
  is used for many purposes which are really very different (so there's a real
  need to clean up the conceptual mess here! - see the detailed comments for
  more on this).

  The language design as a whole could be claimed to be somewhat
  schizophrenic. One design goal that comes to mind (which is not satisfied by
  Modular C) is that a certain short list of properties should be obtained by
  the smallest possible modification of C, such that the migration effort
  would be tiny, and still provide those properties (e.g.,
  encapsulation). Another design goal could be to create a completely new
  language which is "nicer than C, but similar" in some sense. Modular C as
  presented here is in the middle of this spectrum, such that the migration
  effort will (probably) be quite substantial for non-trivial programs, and
  still the resulting program is very much like C (so we work hard to migrate
  the software, and then don't gain all that much).

  Wouldn't it be possible to avoid some of the migration effort with a Modular
  C which is closer to C, and still get (almost) the same benefits? It would
  be interesting to have a discussion about this possibility in the paper. As
  it is now, I get the impression that it will be a big step to switch to
  Modular C, compared to the benefits obtained by doing that. But it's
  possible that a discussion about this topic could argue that the migration
  effort isn't big after all, or it's possible to do it automatically, or
  something like that.

  All in all, this is a very practical paper that offers useful contributions
  in the area of getting to a more "civilized" version of C. It is not very
  precise on the semantics of the proposed language, nor on the metatheory and
  the associated lemmas and theorems, but I trust the authors on their claims
  that it actually works.

Some detailed comments:

- p1: That's a nice and informative abstract! Just one thing sticks out: It is
not obvious how restricting the phrase 'do not use extensive C macro
facilities..' will be in practice. Doesn't almost every macro manipulate
identifiers? Or are we only talking about macros that construct new
identifiers using '##' or something like that? It would be helpful if you
could find a way to describe the same thing which is both accurate and less
broad.

- p2, 'the two defaults that we try to tackle': I'd suggest using something
like 'faults' or 'shortcomings' rather than 'defaults'. In a computer science
context 'defaults' seems to be far too well-known as a word meaning "standard
value or approach that applies when none is explicitly chosen".

- p3, 'struct fields .. are not protected': So how would struct fields be
unencapsulated? Presumably, we are talking about versions of C after C89 (I
believe struct members shared a single name space at some point before
that).

- p4, typo: 'The[y] may be split'?

- p4, '"C-like" .. Java': This might be a statement that creates some
controversy, so maybe you should add a remark about Java which acknowledges
that it is more different from C than, say, C++ is. (There are some syntactic
similarities, but being a "managed" language with known heap safety guarantees
makes Java fundamentally different from languages like C, C++, and
Objective-C).

- p5, typo: 'The lat[t]er allows to create..'

- p5, 'Import .. acyclic': Do you have any data on cycles in the #include
graph of a substantial amount of C systems? It's a non-problem in C to have
'a.c' #include both 'a.h' and 'b.h', and 'b.c' could do the same, and this
might be considered to be a cycle where "a" and "b" mutually depend on each
other. Would that have to be eliminated? Would that be a major roadblock for
anyone trying to use Modular C? If so, do you have any work-arounds? If you're
discussing this later on (writing this, I haven't yet read the rest of the
paper) you could just give a hint about your approach to this topic.

- Listing 1: A quick search (on the following pages) does not reveal any
information about the meaning of line 21. `o` seems to be an undeclared free
identifier. Also, `element` is not declared anywhere I can see. Finally,
`val` in line 26 is apparently a member of `element` which would then be a
struct, but we can't see that. Please make the example more self-contained,
or at least clarify.

- p7, typo: 'is be' --> 'is', 'is intended to be'?

- p7, 'identifiers are mangled': Presumably, this creates an acute need for
patching existing object files and libraries, in particular when no source
code is available. Does your approach allow for linking with "non-mangled"
object files? Later on it is mentioned that `main` is defined via an `entry`
directive. So your approach really requires widespread edit access to the
sources of the software that is to be ported to Modular C, right?

- p8, Section 2.3: It sounds like you can export a feature named N in the
current module under the name N2 using `#pragma CMOD module N2 =
this::module::N`. That's highly confusing. Why don't you call it `export`?
Why can't you export more than one feature? The example with rand seems to
imply that it's possible to export both `srand` as `set` and `RAND_MAX` as
`MAX`, so apparently you _can_ export more than one feature? Then what's the
"emphasis" about, it looks like a rename rather than an emphasis?? Please
clarify.

- p9, Section 2.4: It is not obvious why it would be useful to be able to
change the intra-identifier separator in each importing module. Please add a
sentence to motivate this feature. By the way, is that dash-like character an
ndash? Something ASCII? It seems to break the lexical rules of C no matter
what.

- p9, typo: 'historically include complexity'?? 'the historically included
complexity'? I still don't understand what you mean. Also, it's unclear what
you mean by 'monolithic' header files? Maybe you're saying that there will
only be one header file for a huge amount of software ("all of X windows",
"all of Boost" or something like that)? And the 'exponential number' of other
files: Numbers generally aren't exponential, that's a property of a function,
or a sequence of numbers (which could be considered as a function whose domain
is the natural numbers). So does it just mean that they include "many" other
files? Or are you considering the transitive relationship where A.h includes
B1.h and B2.h and they include C11.h, C12.h, C21.h, C22.h, etc?

- p10, Rule 3.5: This is trivially true for every acyclic graph (even the ones
that aren't connected, although you wouldn't need that). So please state it as
a known property (a "lemma" maybe), not as a 'rule'. In general, it would be
helpful if you would distinguish also 'rule' 3.3 (which is an implementation
choice), 'rule' 3.4 (which is a constraint on the software that developers
must conform to, and tools may check). They should not all be rules! Maybe you
could have 'Implementation decisions', 'Rules' (checked and enforced software
constraints), 'Consequences' or 'Observations' (properties which are known to
hold, given that the rules are enforced), and maybe more.

- p10, Rule 3.6: This is too unspecific. What is the 'import dependency
graph'? (The transitive closure of the modules denoted by `import` directives?
Is that determined by those directives, or could it depend on environment
variable settings, command line arguments, etc, defining "import paths" or
such things?) And the word 'accessible', accessible to what? (Maybe each
module M has an import dependency graph, and said symbols are accessible to
the declarations in M? But I thought that maybe you'd need to
prefix::those::symbols::myModule::S to denote the `S` declared by `myModule`?)

- p10, '`a::b::c`': How do you distinguish the situation where `a::b::c`
denotes a module `c` which has `a::b` as its parent, and the situation where
it denotes a declaration named `c` in the module `a::b`? Is it possible for a
third party to declare `a::b::c` as a new module that extends `a::b`, or can
you know for sure that such a "child" module can/will only be declared by the
organizational entity that declared `a::b`? In that case, will it be a
breaking change if `a::b` is modified to include a declaration of something
named `c` when there is already a module `a::b::c`? The sentence 'In
particular, the identifier `a::b::c` could already have a definition' seems to
imply that this type of conflict could arise. Note that the authors of `a::b`
may not need whether there's already a module `a::b::c` out there in the
world of third party developers using (and extending) `a::b`.

- p10, 'Our reference implementation separates name components with a hyphen':
Confusing! Does this mean that all your examples using '::' should instead be
written with '-' in order to work with your reference implementation? Please
clarify (or, maybe better, just don't mention this detail, if it doesn't help
the reader understand your ideas anyway; it could be in a README file
distributed along with your reference implementation).

- p12, 'stands for the name of the importing module': This is weird! The name
`T` is used as a type in the snippet of code that follows, so why wouldn't it
be expected to be a type? How could it even work in the case `T` does not
stand for a type? Also, what is an "incomplete type"? Please clarify! It
sounds a lot like your snippets are very similar to C macros used to declare
functions (and maybe types?), but as mentioned, several things about them are
deeply confusing.

- p12, typo: 'The lat[t]er implies'. (Maybe check out
http://grammarist.com/usage/later-vs-latter/)

- Section 4.1-3: The semantics of snippets is not clear at all. Please try to
explain what the basic idea is (maybe it's macro-like, that the `snippet`
directive itself introduces a piece of code, and, somehow, certain parts of
that code can be replaced by importers, similar to macro parameters, but it's
completely unobvious whether that's anywhere near the truth).

- p14, typo: 'who's' --> 'whose'

- p15, Definition 5: How is the preprocessed copy of P obtained? You can't
define "for given P, F(P) consists of P2 where ..": If we don't know how to
produce P2 from P then there is no way we can start subjecting P2 to all the
modifications specified as "where ..". Or maybe you intend to specify that we
perform those transformations (replace certain definitions by certain
declarations, etc) on _P_, and the result of that transformation is the
preprocessed copy you mention? Please disambiguate. Maybe you could say
something like "the declaration extract is obtained from P by replacing all
...". For instance, Definition 6 has that structure.

- p16, 'functionally equivalent': This is of course an undecidable property in
general. Couldn't you specify that they should be syntactically equivalent,
maybe via some "erasure" on both?


----------------------- REVIEW 5 ---------------------
PAPER: 3
TITLE: Modular C
AUTHORS: Jens Gustedt

Overall evaluation: -1 (weak reject, some problems)

----------- Review -----------
This paper proposes an extension to the C standard.  The extension
called Modular C adds name spaces to avoid problems caused by
traditional C preprocessing.

The proposed extension seems well designed and practically useful
but it is somewhat naive as academic publication.  There are many
module systems (or name space) proposed so far.  What are new ideas?
The presentation of details of the specification could be improved.

Section 2, 1st paragraph:

The first paragraph could be revised to clarify the semantics.
What is the semantics of "CMOD module"?  It has to be clearly
stated here.  For example, if you "CMOD import" C::io, then
it is obvious that C::io is a module name.  Why do you have to
"CMOD module" it?  Doesn't it declare C::io is a module name?
Why do you need both "CMOD module" and "CMOD import"?

Furthermore, in Listing 1, in line 18, C::io::printf is called.
So it is obvious that the C::io module has to be imported.
Why do you have to explicitly say "CMOD import"?  When do you
have to do so?

It would confuse the readers that the paper does not clearly
distinguish name resolution and linking.  Selecting a particular
function among the functions with the same name but belonging to a
different module is slightly different from selecting compiled
binaries to be linked.  This paper seems to call both "import".
This is confusing.

Rule 2.4:

Please clearly mention differences between "definition" and
"declaration".  What is the definition of an identifier in C?
Instantiation?

Section 2.2:

The syntax of "CMOD import" is different from the one in Section 2.
This fact is confusing.  Here, does "CMOD import" introduce
an alias of module name?  Please present the semantics of
those pragmas.

It was not clear why "CMOD composer" is necessary.  OK, you might
want to use "macro-0()" but why is "macro_0()" bad?  Although
more flexibility is better, too much flexibility might cause
unexpected errors.  For example,

#pragma CMOD composer -
#define x-y()  foo()
int x = 0;
int z = x-y();  // "z = x - y()" or "z = foo()"?

How is this code read?

Section 3:

It might be better to present concrete examples of horizontal dependency
and vertical dependency in the context of the C language.

Section 3, page 9, the last paragraph:

Why is parsing C++ headers slower than parsing C headers?  Because of
templates?  Please mention the reasons.

p.11, the third paragraph:

"In contrast, to this approach, the C standard offers"
=> "In ..., the C++ standard offers" ?

The proposed approach may have a drawback as well.  Although once_flag
allows postponing initialization to appropriate times, the proposed
approach forces all initialization at the start-up time.  This might
cause a slow start.

