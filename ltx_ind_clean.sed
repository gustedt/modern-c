s!|hyperpage!!g
s!{\(\\color  {[[:alnum:]]*}\)*\\ttfamily  \\bfseries *!\\tcode{!
s!\\char 95\\relax !_!g
s!\\textunderscore !_!g
s!\\unhbox \\voidb@x \\kern \\z@ *}!}!g
s!\\unhbox \\voidb@x \\kern \\z@ *! !g
/[.]L/{
        s-library!macro-label-
        s!macro!label!g
}
/obsolete/{
        /gets/!{
                s-library!obsolete-label-
                s-(obsolete)-label-g
        }
}
/type/{
        /\<[a-z][a-z0-9]\{2,2\}\>\|\<xmm/{
                s-library!type-hardware register-
                s-type-hardware register-g
        }
}
/pragma/{
        /deprecated\|fallthrough\|maybe_unused\|nodiscard\|noreturn\|unsequenced\|reproducible/{
            s-pragma-attribute-g
            s-__\(deprecated\|fallthrough\|maybe_unused\|nodiscard\|noreturn\|unsequenced\|reproducible\)__-\1-g
        }
}
/\<t[mv]_\|quot\|rem/{
        s-library!tagname-library!struct member@\\code{struct} member-
        s!tagname!\\code{struct} member!g
}
# some parasites
/Rewind/d
s!{|!{"|!
s! \{1,\}@!@!g
s!\\code {!\\code{!
s!\\code\>!\\tcode!g
