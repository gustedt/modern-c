#!/bin/sh

sed "
  s![\\]000!!g
  s![\\]040! !g
  s!darkblue !!g
  s!\[basicstyle=.*\]!!g
  s![\\]xspace! !g
  s![\\][[:digit:]]\{3,3\}!!g
"
