/* This shows how SWAP would be implemented with lambdas as found in
   C++. */

#define SWAP(X, Y)                                              \
  (                                                             \
  /* This starts the lambda construct. */                       \
  [                                                             \
   /* These two captures play the role of function              \
      parameters and read X and Y ... */                        \
   swap_p1 = &(X),                                              \
   swap_p2 = &(Y)]                                              \
  /* Then effectively the parameter list is empty */            \
  (void) { /* now the body starts */                            \
    static_assert_compatible(*swap_p1, *swap_p2,                \
                             "to exchange values, '"            \
                             #X "' and '" #Y                    \
                             "' must have compatible types");   \
    auto swap_tmp = *swap_p1;                                   \
    *swap_p1 = *swap_p2;                                        \
    *swap_p2 = swap_tmp;                                        \
  }                                                             \
  /* Now this lambda value can be called immediately with */    \
  /* no arguments */                                            \
  ()) /* end of the expression */

#define isnegative(X) ((X) < (typeof(X)0))

#define issigned(X) isnegative(typeof(X)-1)

#define mincharacteristic(X, Y) ((issigned(X)<<2)|(issigned(Y)<<1)|1)

#define minunsigned(X, Y) (_Generic(tozero(X)+tozero(Y), typeof(X): tozero(Y), default: tozero(X)))

#define minreturn(X, Y)                                 \
  _Generic(                                             \
           (char(*)[mincharacteristic(X, Y)]){ 0 },     \
           /* both signed, arithmetic conversion */     \
           char(*)[4|2|1]: tozero(X)+tozero(Y),         \
           /* only one signed, use it */                \
           char(*)[0|2|1]: tozero(Y),                   \
           char(*)[4|0|1]: tozero(X),                   \
           /* both unsigned, use narrower */            \
           char(*)[0|0|1]: minunsigned(X, Y)            \
)

#define MIN(X, Y)                                                       \
  (                                                                     \
  /* This starts the lambda construct. */                               \
  [                                                                     \
   /* These two captures play the role of function                      \
      parameters and read X and Y ... */                                \
   min_x = (X),                                                         \
   min_y = (Y)]                                                         \
  /* Then effectively the parameter list is empty */                    \
  (void) { /* now the body starts */                                    \
    typedef typeof(minreturn(min_x, min_y)) min_type;                   \
    /* Effectively one of the three cases is determined                 \
       at compile time. */                                              \
    return ((isnegative(min_x) && !issigned(min_y))                     \
     ? (min_type)min_x                                                  \
     : ((isnegative(min_y) && !issigned(min_x))                         \
        ? (min_type)min_y                                               \
        : (                                                             \
           /* both have the same signedness or are both positive */     \
           (min_x < min_y)                                              \
           ? (min_type)min_x                                            \
           : (min_type)min_y)));                                        \
  }                                                                     \
  /* Now this lambda value can be called immediately with */            \
  /* no arguments */                                                    \
  ()) /* end of the expression */
