/* This shows how SWAP would be implemented with blocks as found in
   Objective C. */

#define SWAP(X, Y)                                              \
  (                                                             \
   /* This starts the block construct. */                       \
   void ^                                                       \
   /* We have no other parameters, so a void list */            \
   (void) { /* now the body starts */                           \
     /* These two variables have the role of function           \
        parameters. They ensure to evaluate the                 \
        expression X and Y once, since these could be           \
        complicated lvalue expressions with evaluated           \
        subexpressions that have side effects. */               \
     auto const swap_p1 = &(X);                                 \
     auto const swap_p2 = &(Y);                                 \
     static_assert_compatible(*swap_p1, *swap_p2,               \
                              "to exchange values, '"           \
                              #X "' and '" #Y                   \
                              "' must have compatible types");  \
     auto swap_tmp = *swap_p1;                                  \
     *swap_p1 = *swap_p2;                                       \
     *swap_p2 = swap_tmp;                                       \
     }                                                          \
   /* Now this block can be called immediately with */          \
   /* no arguments */                                           \
   ()) /* end of the expression */
