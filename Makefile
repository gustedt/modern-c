# In every branch the following variable should point the branch that
# is the origin of it and from which diffs should be produced:

REFERENCE ?= current
# Use this for production diffs
DIFFSTYLE = INVISIBLE
DIFFSUB   = SAFE

SOURCES =						\
	annex.tex					\
	atomic.tex					\
	code/snippets/DEFAUL1.tex			\
	code/snippets/FLT_RDXRDX.tex			\
	code/snippets/circular.tex			\
	code/snippets/circular_append.tex		\
	code/snippets/circular_delete.tex		\
	code/snippets/circular_destroy.tex		\
	code/snippets/circular_element.tex		\
	code/snippets/circular_getlength.tex		\
	code/snippets/circular_init.tex			\
	code/snippets/circular_new.tex			\
	code/snippets/circular_pop.tex			\
	code/snippets/circular_resize.tex		\
	code/snippets/draw_sep.tex			\
	code/snippets/enable_alignment_check.tex	\
	code/snippets/fgetline.tex			\
	code/snippets/fprintnumbers.tex			\
	code/snippets/heron.tex				\
	code/snippets/mbrtow.tex			\
	code/snippets/mbsrdup.tex			\
	code/snippets/mbsrlen.tex			\
	code/snippets/mbsrwc.tex			\
	code/snippets/numberline.tex			\
	code/snippets/sprintnumbers.tex			\
	cognition.tex					\
	errorcheck.tex					\
	experience.tex					\
	modernC.tex					\
	modernCcoding.tex				\
	modernCfunctionpointers.tex			\
	modernCmemorymodel.tex				\
	modernCpointers.tex				\
	restrict.tex					\
	string-processing.tex				\
	threads.tex

DISTHTML =					\
	org-style.css				\
	bugs.html				\
	download.html				\
	forum.html				\
	README.html

DISTPNG =					\
	fig/Gustedt-ModernC-HI.jpg		\
	fig/jay.png				\
	fig/chough.png				\
	fig/raven.png

DIST = gforge:/home/groups/modernc/htdocs

# This needs the difflatex command to be installed.
DIFFLATEX = latexdiff-git --debug --allow-spaces --math-markup=0 --driver=pdftex --type=${DIFFSTYLE} --subtype=${DIFFSUB} --exclude-safecmd=difflatex-safecmd-exclude.txt --append-safecmd=difflatex-safecmd-append.txt --append-context1cmd=difflatex-context1cmd-append.txt
#"PICTUREENV=(?:picture|DIFnomarkup|shortlisting|lstlisting|eqnarray\*)[\w\d*@]*"
#DIFFLATEX = latexdiff-git --allow-spaces --math-markup=0 --driver=pdftex --type=${DIFFSTYLE} --subtype=${DIFFSUB}

# You may temporarily overwrite this by giving REFERENCE=OVERSION
# before the make command, where OVERSION is some tag, branch or other
# SHA that git knows about.
#
# To produce the diff'ed version to "OVERSION" inside a new directory
# "diffOVERSION" run
#
# make diffOVERSION
#
# this is very verbose, don't be scared.
#
# Once this directory is produced, do
#
# cd diffOVERSION
# make -B all-diffOVERSION.pdf
#
# This should also be run three times to produce all toc's, indexes
# etc

modernC.pdf : *.tex modernC.ind
	-mv modernC.pdf modernC-prev.pdf
	-pdflatex  -file-line-error   -interaction=nonstopmode "\input" modernC.tex
	-sed -i -f ltx_toc_clean.sed modernC.toc
	-cp modernC.pdf modernC-prev.pdf

Cmodules.pdf : *.tex
	-mv Cmodules.pdf Cmodules-prev.pdf
	-pdflatex  -file-line-error   -interaction=nonstopmode "\input" Cmodules.tex
	./clean-out.sh < Cmodules.out > Cmodules.out-new
	-mv Cmodules.out-new Cmodules.out
	-cp Cmodules.pdf Cmodules-prev.pdf

index :
	sed -i -f ltx_ind_clean.sed modernC.idx
	makeindex modernC

toc :
	sed -i -f ltx_toc_clean.sed modernC.toc

TAGS : ${SOURCES}
	etags ${SOURCES}

modernC-code.zip :
	rm -f modernC-code.zip
	zip modernC-code.zip modernC-code/*.[ch] modernC-code/build-musl modernC-code/Makefile modernC-code/LICENSE modernC-code/README modernC-code/AUTHORS

diff${REFERENCE} :
	-rm -f diff${REFERENCE}/*.tex
	${DIFFLATEX} -d -r ${REFERENCE} ${SOURCES}
	cp -f Makefile *.sh *.sed *.sty *.cls *.bib diff${REFERENCE}
	ln -s -f ../code diff${REFERENCE}/code
	ln -s -f ../fig diff${REFERENCE}/fig

distribute :
	scp ${DISTHTML} ${DIST}/
	scp ${DISTPNG} ${DIST}/fig/

#( cd diff${REFERENCE} ; for f in ${BADDIFF} ; do sed -i '/clearpage/s!.*!& (no diff marks, here)!' $$f ; done )

