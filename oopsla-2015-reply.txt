Dear PC members and reviewers. Since I don't see much of a chance that
the outcome of 3 "reject" can (or even should) be changed by a
rebuttal, I take this as a opportunity to thank you all for the
careful reading that you have given to my submission. I really
appreciate the surprisingly long and detailed reviews that I received.

Since the outcome will not change, I'd also like to comment or answer
to some of your remarks and questions. Since the reviews were long,
the answer is a bit long, too, and way above the intended limit of 500
words. I hope you don't mind.

++++++++++

For reviewerA, while I agree with (1) as one target of application of
Modular C, I don't agree with the offered choice (2) at all. This
supposes that C++ (a permanently moving, experimental language with
feature overkill) or Go and maybe Objective C (languages that are
proposed by some dominating industrials in our domain) would be viable
choices in all cases.

C is not just a "legacy", is a language that is under active (but
slow) development. It is in many cases chosen for new and even large
software projects, because maintenance is easier, forward and backward
compatibility is guaranteed and developers with decent skills are
easier to find.

 A> Rule 3 - Can the headers still be separated from the TU code?

Yes, the name mangling can be parametrized to produce an interface
with human readable identifiers. This is should be explained with the
migration path, probably I should work that out clearer.

 A> Since the directives are not typed, if you have a conflict, ...
 A> ... Will it be your tool or perhaps the compiler that handles this
 A> error?

Modular C is essentially text replacement and only works on the level
of identifiers. As a consequence these types of errors will usually be
diagnosed when the conventional compiler sees them, not before.

 A> It would seem that this is chosen so that Modular C parses slower,

No, for all that I observed so far with the system, parsing time
itself is simply negligible and mangling protects from name space
pollution. I think that extraction of the effectively needed
interfaces would slow down compilation.

 A> module_* is now a reserved keyword.

A reserved prefix. All such conventions would be obviously open for
discussion.

 A> Why not follow the convention of
 A> Java and C++ and use the module name as a constructor?

What do you mean by constructor? There is not necessarily a data type
that is associated to the module.

Also, the module name itself already has a specific role in this
system.

 A> When is the type parameter/type T in a snippet instantiated?

on import compilation

 A> You can "import" a snippet and if so a snippet is not instantiated
 A> at compile time (c.f. Section 3.1),

A snippet has two "compile times", the one of the exporter (where it
essentially left alone) and the one of the importer.

 A> in this case are snippets type safe

Yes, they are textual replacements that are completely type checked at
compile time (of the importer). C is a statically typed language and
this proposal doesn't change this.

Modular C actually adds to the type safety, since it can (and should)
be used to avoid "void*", untyped macro, function pointer hacks and
pointless casts or conversions.

 A> Would it still be possible to define global variables?

The term "global variable" is not well specified here.

- external symbols? Obiously, yes.
- un-prefixed exported identifiers in file scope? No.
- un-prefixed static identifiers in file scope? Yes.

 A> The mimic keyword and the description of migration path in Section
 A> 8 suggests existing binary can be linked with Modular C programs,

yes (although, I was not sure if this should be part of the "core"
proposal or just be a feature of the existing implementation to bind
to an external library, here the C library)

 A> given a specially crafted header file for that binary. I find this
 A> contribution most important in this extension.

Ok, thanks, so I'll definitively move that to the core of the proposal.

 A> An analysis of the
 A> overhead of translating structured C library calls to actual calls
 A> would be appreciated.

This is just link time aliasing of symbols, overhead is negligible at
compile time and non existing at run time.

 A> Q: Where do you put #pragma CMOD definition in the new header
 A> files, and would this violate Rule 3 to have a separate header
 A> file?

Generated header files are traditional C and don't refer to the module
system as such. They interface all hierarchical names by trivially
mangled names, e.g where :: is simply replaced by _

 A> Finally, is it possible for Modular C to construct anonymous
 A> modules,

no

 A> or a module where all members are in the global scope
 A> like normal C objects?

no

 A> The may sound counter intuitive

It is, and it also is counter productive, since it would immediately
recreate naming conflicts, where the essence of this proposal is to
eliminate these.

For "private" file scope identifiers C already has the "static"
keyword. All non-external identifiers are left as they are by Modular
C.

 A> for a
 A> Modular C extension, but having a flat/global object hierarchy in
 A> C has the advantage of the relative ease for other languages to
 A> link with C objects

We don't need that, since the hierarchical names can automatically
be translated to "normal" C names as described above.

 A> ... but your approach effectively introduces a new C-like language.

It can probably be read as such. But the only real syntax difference
that is added to the code itself are structured identifiers, and these
can (and are) trivially replaced by a text processing tool.

The difference to other such "new" languages is that we have a clear
migration path from existing C to Modular C, and that an automatic
header export to interface with existing C code (and thus other
languages, such as C++, Fortran or Objective C) is always provided.

 A> A user would be thinking if Modular C is not
 A> compatible with C and I need to annotate 3rd party header files
 A> just for my code to use an existing binary, why don't I use C++
 A> instead which has all these features built-in?

``Why not to use another language as C++'' is a whole different
debate, see above. This is not a valid argument: there are many other
factors (technical, social, political) that determine if a programming
language is suitable for a specific project.

 A> ### Lack of related works

Thanks for the pointer, I will definitively look into this.

 C> - Many people make a distinction between information hiding (name
 C> hiding, statically) and encapsulation (representation hiding,
 C> dynamically). This paper uses encapsulation, but usually means
 C> information hiding.

I am not sure that the last phrase is correct, and even if the
difference between "static" and "dynamic" is applicable,
here. Abbreviations are "name hiding" at the surface, but as we
describe e.g for C::io versus POSIX::io it can easily be used for
"representation hiding". It allows, e.g to seamlessly replace one
implementation of an interface contract by a completely different
implementation, that uses different data structures, algorithms
etc. To do so you'd just have to change one identifier in the source
of the TU and then recompile. This is "static" because it is fixed,
once compiled, but it also is dynamic because it is easy to change.

I still think that "encapsulation" better describes these properties.

 C> - Page 8: for symmetry I would chose "module_at_exit" and
 C>   "module_at_quick_exit".

I would, too, but this refers to standard C library interfaces atexit
and at_quick_exit, which are unfortunately named as they are.

 C> - Page 9: "If we want several specialization within modular C, we have
 C>   to create separate modules for each of the cases that we want to
 C>   cover. We consider this a feature"
 C>   I don't find this convincing - this seems rather cumbersome.

It just makes all used features and their place of specialization /
instantiation explicit. It may in fact be cumbersome at a first look,
but helps to avoid the combinatorial explosion that we encounter for
C++ code compilation. A stub module that implements a specialization,
acts the same as a pre-compiled header file.

All in all, my personal opinion is that all that invisible function
and operator overloading of other languages is a Troyan horse for bugs
and compile- and run-time performance problems. I don't want to open
that breach for C: C is explicit programming.

 C> - Page 14: There is no conclusion and future work. What is next?

Discussion. This proposal that will only fly if it is exposed to the
public, and undergoes a process of accommodation. This was my main
motivation to submit to OOPSLA.

 C> Are you proposing this to the C standards body?

Yes, I am a member of the ISO C committee and will propose this once
we will enter the phase for the next standard. (Perhaps at the autumn
meeting this year.)
