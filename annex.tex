\appendix
\startonoddpage
\chapter*{Technical Annex}
\label{level:annex}
\renewcommand{\thesection}{\Alph{section}}

If you read this book much after 2024, this technical annex
should not concern you: you should have a development framework at
your disposal that implements \C{23} without fault. If not, you
might have some difficulties because one or another feature of \C{23}
is not yet implemented. The following site tracks the progress
of \C{23} conformance of several toolchains:
\begin{center}
  \url{https://en.cppreference.com/w/c/compiler_support}.
\end{center}

My personal experience with programming in C is almost entirely
acquired in the realm of open source projects and open source
compilers (\code{gcc} and \code{clang}) and on specific operating systems---
namely,~\cite{POSIX2024}(2024) in its incarnation as glibc/Linux and
musl/Linux. If this is not your setting and you encounter severe
problems with the code in this book, you'll have to search the web for
solutions, as I and the remainder of this annex will probably not
be of much help.

\startonoddpage
\section{Transitional code}
\label{sec:transitional}

{\lstindexbf%
\C{23} comes with a lot of new features, and at the beginning, we
cannot expect that all compilers will catch up quickly to implement
all new features. Nevertheless, many of these new and closely related features
are already present in compilers as
extensions. Therefore, in the sample code provided with this
book at
\begin{center}
  \url{https://inria.hal.science/hal-03345464/document},
\end{center}
we provide a header file \Cinclude{c23-fallback.h} meant to
circumvent most of the difficulties you might encounter. It is
systematically included in our examples, such that these should, in
general, compile even in legacy settings. %
}%
Nevertheless, some \C{23} features are required, namely,

\medskip%
\begin{itemize}
\item Digit separators such as in \code{0xAB'CD}
\item Binary integer literals such as in \code{0b1010} or \code{0B0101}
\item The new attribute syntax such as in \code{[[deprecated]]}
\end{itemize}

\medskip%
\noindent%
The first two are not easy to circumvent, so you should probably not
even try to use \C{23}-enabled code without having support for them.

For the attribute syntax, there is a feature test:
\code{__has_c_attribute}. It can be used with define (or
\code{\#ifdef}) to test for the syntax itself and with \code{\#if} and
an argument to test for an individual attribute:
\lstpartial{319}{321}{code/c23-fallback.h}

Another feature test is \code{__has_include}, which similarly can be
used to test for the preprocessor feature itself and, if that is
available, to test for the availability of a specific header. This can
be used to

\medskip%
\begin{itemize}
\item Test for optional headers such as \Cinclude{complex.h},
  \Cinclude{threads.h}, and\\
  \Cinclude{stdatomic.h}. In principle, this
  now allows us to use the corresponding feature test macro
  \begin{center}
    ({\code{__STDC_NO_COMPLEX__}} {\code{__STDC_NO_ATOMICS__}} {\code{__STDC_NO_THREADS__}})
  \end{center}
  to test for compiler support of these features separately from the
  availability of library interfaces.
\item Test for new headers that come with \C{23}, namely,
  \Cinclude{stdckdint.h} and \Cinclude{stdbit.h}:
\end{itemize}
\lstpartial{563}{567}{code/c23-fallback.h}
Our fallback header uses this throughout but always ensures that each
test for a specific file is protected by a test for the feature
itself. In general, this feature is already present in many pre-\C{23}
compilers, so it should not be much of a problem.

This header also unconditionally includes a bunch of C library headers
(see the following discussion) and augments them with \C{23} features if possible. You
should not include them yourself so we are sure of what we
get. This also concerns the new \C{23} header \Cinclude{stdckdint.h}
as seen previously.

Other headers may also be included conditionally to emulate missing
features (namely, \Cinclude{threads.h} and \Cinclude{stdatomic.h}).

When emulating the new features, there may be spurious warnings that
some attributes are misplaced or ignored. This obviously means that
they are not completely taken into account and that the analysis they
are expected to provide is not yet fully implemented by your
compiler. If you are flooded by these, you might switch some of the
diagnostics off by defining the macro \code{C23_FALLBACK_SILENT}, for
example, by providing the command line argument
\texttt{-DC23\_FALLBACK\_SILENT} to the compiler. Don’t forget that
this header is only meant as a transitional feature; don't expect it
to work forever.

\begin{reminder}
  Only use the header \texttt{c23-fallback.h} transitionally, until
  your platform fully supports \C{23}.
\end{reminder}
\begin{reminder}
  Header \texttt{c23-fallback.h} only emulates some \C{23} features
  with restricted capabilities.
\end{reminder}
So it may be a good idea to use this header to get started, but once
your compiler(s) fully support \C{23}, you should remove this crutch.

There are some more features provided by this header, in particular,
feature tests. First, to test for the new \code{__VA_OPT__} feature, use

% define __has_va_opt
\functionDocu{__has_va_opt}{155}{164}{code/c23-fallback.h}

\noindent%
And then, for a whole bunch of other minor features that are provided
as extensions to \C{23}:


\begin{multicols}{2}
\lstinline`iscomplex`\\
\lstinline`isimaginary`\\
\lstinline`isdecimalfloating`\\
\lstinline`iscomplex`\\
\lstinline`isstandardrealfloating`\\
\lstinline`isstandardfloating`\\
\lstinline`isfloating`\\
\lstinline`iscompatible`\\
\lstinline`is_potentially_negative`\\
\lstinline`is_const_target`\\
\lstinline`is_volatile_target`\\
\lstinline`is_const`\\
\lstinline`is_volatile`\\
\lstinline`is_null_pointer_constant`\\
\lstinline`is_zero_ice`\\
\lstinline`isinteger`\\
\lstinline`issigned`\\
\lstinline`isunsigned`\\
\lstinline`isice`\\
\lstinline`isvla`\\
\lstinline`isxwide`\\
\lstinline`is_pointer_nvla`\\
\lstinline`is_pointer_vla`\\
\lstinline`is_pointer`\\
\lstinline`is_array`\\
\lstinline`is_fla`\\
\lstinline`is_void_pointer`
\end{multicols}

\noindent%
If you are interested in these, please look into the source of the
fallback header to see what they do and how they are implemented.

\startonoddpage
\section{C Compilers}
\label{sec:compilers}

As of this writing (March 2024) \code{gcc} and \code{clang} in their latest versions
implement most of the new language features of \C{23}, but
unfortunately, some notable features are still missing: for Clang 18, the
\code{constexpr} storage class specifier, and for Clang 18 and GCC 14,
\code{[[unsequenced]]} and \code{[[reproducible]]} attributes.

Using less recent compilers also works for large parts, but the less
recent, the more problems you will have, particularly if you need to use 128
bit types (see the following discussion). It seems that starting from GCC 10 and Clang 14,
the C23 support is reasonable.

The code used in this book should compile when using the
fallback header as indicated. In any case, it is always better to use
the latest compiler release supported by your platform. Not
only will it better implement the C standard as we need it for \C{23}
support, it will also better optimize and take advantage of modern
hardware.

\begin{reminder}
  Use the most recent compiler release.
\end{reminder}

\subsection{Attributes}
\label{sec:missing-attributes}

\noindent%
For the the \code{[[unsequenced]]} and \code{[[reproducible]]}
attributes, both compilers implement extensions
\code{__gnu__::__const__} and \tcode{__gnu__::}\\\tcode{__pure__} that come close
to these features. They can be used either with the attribute
extension that \code{gcc} had used previously
(\code{__attribute__((__const__))} and
\code{__attribute__((__pure__))}) or with the new \C{23} syntax with a
\code{gnu::} prefix. We do not provide an in-place fallback to them
because their syntactic placement is different than for the C standard
attributes. These apply to types, whereas the extensions apply to
declarations. Nevertheless, we provide \code{c23_unsequenced} and
\code{c23_reproducible} as shortcuts for these extensions.

\subsection{Missing \textbf{\texttt{\#embed}}}
\label{sec:missing-embed}

Unfortunately, as of March 2024, the \code{#embed} feature does not
seem to have arrived in the compilers that we have at our disposal. If
you want to experiment already with this feature, there is a project
that emulates it that can be used relatively easily:
\begin{center}
  \url{https://sentido-labs.com/en/library/\#cedro}.
\end{center}

The \texttt{Makefile} that comes with the example \texttt{code}
directory shows examples of how to use this program to provide the
functionality of \code{#embed}.

\subsection{Missing \textbf{\texttt{constexpr}}}
\label{sec:missing-constexpr}

For the lack of \code{constexpr} in \code{clang} for integer types, there is
indeed a replacement based on a much wider definition of
integer constant expressions. Namely, \code{clang} accepts names of
\code{const}-qualified objects with compile-time initializers also as
integer constant expressions. So, for a large range of applications,
basically cheating for this particular compiler by doing the following
is almost sufficient:

\lstpartial{250}{255}{code/c23-fallback.h}

Here, \code{__is_identifier} is a comfortable \code{clang} extension that
tells us whether its argument (here, \code{constexpr}) is an identifier
(the result is $1$) or a keyword (the result is $0$). So, whenever \code{clang} will
be ready for this and returns $0$---probably somewhere in release 18 or
19---this code is ignored automatically. We also give code that
emulates \code{__is_identifier} for other compilers. Again, if you are
interested, you can look up the code in the fallback header.

\subsection{Missing 128-bit integer support}
\label{sec:missing-128-bit-integer-support}

\code{gcc} has partial support for 128-bit integer types, even on platforms
with 64-bit hardware types, for a long time. Before \C{23}, it had not
been possible to integrate these into the standard-supported integer
types because they are wider than the once-chosen \code{[u]intmax_t}
types.

\C{23} makes the following types accessible as \code{[u]int128_t}
types if all prerequisites for these types are met:

\medskip%
\begin{itemize}
\item Macros and types in \Cinclude{stdint.h}
\item Macros in \Cinclude{inttypes.h}
\item Support for \code{"\%w"} \code{"\%wf"} length specifiers for
  \code{printf} and \code{scanf} functions
\end{itemize}

\medskip%
\noindent%
If the latter two are missing, this cannot be fixed with a fallback
header, obviously, but we try to provide the types and macros of the
first set as far as this is possible. For library support, see the following discussion.

There is one particular caution, though, that you should be aware of.

\begin{reminder}
  The support for \code{[u]int128_t} is disabled for clang versions
  before clang-18.
\end{reminder}

This is because the Clang support before that has two serious
incompatibilities that make them dangerous to mix with code that has
been compiled by GCC. These incompatibilities are as follows:

\medskip%
\begin{itemize}
\item On some platforms, the alignment of the \code{[u]int128_t} types
  is different from the ABI of the platform.
\item On some platforms, parameter passing of \code{[u]int128_t} types
  may pass one half in a hardware register and the other on the stack.
\end{itemize}

\medskip%
\noindent%
So, if you want or have to use 128-bit integer types, move at
least to Clang 18; you should not regret it.


\startonoddpage
\section{C Libraries}

Not surprisingly, C~library implementations lack most of the
support for \C{23} for now. In the following, we discuss some of the
additions that come with \C{23}. At the end, section~\ref{sec:musl},
we will report on a project for Linux systems that you can use
temporarily until your C implementation provides full support for the
library parts of \C{23}.

\subsection{Functions borrowed from POSIX or similar systems}
\label{sec:POSIX-functions}

The following functions are harmonized with POSIX:

\begin{itemize}
  \item \code{strftime}
  \item \code{gmtime_r}
  \item \code{localtime_r}
  \item \code{memccpy}
  \item \code{strdup}
  \item \code{strndup}
\end{itemize}

So, if you are on such a system (such as Linux or macOS) or program in
a compatible environment, you should already have these
functions. Another candidate for a function you will most likely
already find on such systems is \code{timegm}.

\subsection{Improved UTF-8 support}

\C{23} provides the new functions \code{mbrtoc8} and \code{c8rtomb}
that have a similar role as, for example, \code{mbrtoc32} and
\code{c32rtomb}, only that they use UTF-8 instead of UTF-32. If you
are among the more experienced readers, implementing these interfaces
would, in fact, be a very good exercise. UTF-8 is a really nice and
sophisticated coding scheme that would teach you a lot,
and correctly implementing these functions has the right level of
challenge in terms of programming and in terms of understanding an
international standard.

\subsection{Bit utilities}

The new header \Cinclude{stdbit.h} provides a lot of interfaces for
bit operations on unsigned integer types (see
\ref{sec:integer-arithmetic}). In particular, most have type-generic
versions that are relatively easy to use and interface.

Some of these or similar functions are, in fact, present as built-ins on
major compiler implementations, so our fallback header provides them
where that seems easily possible. Those provided by the
fallback are the type-generic interfaces.

A complete implementation is already available with
\texttt{glibc-2.39}.

\subsection{Checked integer arithmetic}

The \C{23} header \Cinclude{stdckdint.h} provides three type-generic
macros for checked arithmetic
(see~\ref{sec:integer-arithmetic}). Again, many compilers already
provide equivalent tools so the fallback header makes them available
where we know about this.

\subsection{Formatted IO}

The \code{printf} and \code{scanf} families of functions gain support
for \texttt{w} and \texttt{wf} width specifiers and for \texttt{b} and
\texttt{B} binary number formats.

A complete implementation supporting standard integer types is already
available with \texttt{glibc-2.39}.

The support for extended types---in particular, 128-bit integer types---
is yet failing. Not only is there no support in the library to in- or
output these types, but also the compilers issue confusing warnings when
they are presented with format strings with \code{"\%w128"} or
\code{"\%wf128"} specifiers. So, even if you use a C library that
properly supports the types (see~\ref{sec:musl}), you might get
drowned in warnings about the formats.

\subsection{Mathematical functions}

C23 has a lot of new functions in \Cinclude{math.h}, some of which are
probably only of interest to a smaller community. Complete
implementations of all of these will probably take their time to
arrive in distributions.

The CORE-MATH project
(\href{https://core-math.gitlabpages.inria.fr/}{https://core-math.gitlabpages.inria.fr/})
provides some of the missing features---in particular, the ``pi''
functions, which are trigonometric functions and their inverse that
compute their arguments and return values in half-revolutions in the
interval $[-1, +1]$ instead of $[-\pi, +\pi]$. For overviews of this
project, see \cite{sibidanov:hal-03721525} and
\cite{gladman:hal-03141101}.


\subsection{A reference implementation for musl libc}
\label{sec:musl}

For Linux users, the code directory also has a script file
\texttt{build-musl}. The brave among you may use it to compile your
own version of \texttt{musl} libc with most of the support for C23. At
its start, it contains a brief explanation and information on where to
download the sources for this upgraded version of \texttt{musl}.

This patch-set in particular adds full support for the types
\code{[u]int128_t} on architectures that have GCC's \code{__int128}
extension (including alignment and calling conventions). So, if you are
keen to use these as C23 foresees them, this is for you.

Note, though, that these patches probably have not been fully tested
and may still have bugs. Once they are properly reviewed, most of
these patches will hopefully be integrated into mainstream
\texttt{musl}.
