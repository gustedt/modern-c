---
title: Third edition of Modern C
author: Jens Gustedt
self_contained: yes
date: "${DATE}"
toc_depth: 5
---

The main purpose of this third edition is the update to C23;

## <del>Integers</del>

### <del>standard integer types</del>

#### <del>two's complement</del>

- <del>shorten 5.7, binary representations, where it talks about other sign
  representations</del>
- <del>speak of width and use `UINT_WIDTH` etc before `UINT_MAX`</del>
- <del>introduce the undefined part as a different subchapter, change the
  undefined loop example such that it uses `do_something(i)` and such
  that the implicit replacement is</del>

```C
for (signed i = 1; ; ++ i) do_something(i);
```

#### <del>wide integer types such as `uint128_t`</del>

- <del>also in 5.7 for the `uintXX_t` also introduce the `_WIDTH` macros</del>
- <del>add `uint128_t` as a type that is "likely" to be implemented on most
  platforms</del>
- <del>provide fall back for gcc and alike in a header file, such that
  users can already experiment with it.</del>

### <del>bit-precise integer types<del>

#### <del>introduce `_BitInt(`*N*`)`</del>

- <del>add a new subsection after 5.7.6 that introduces these types</del>
- <del>introduce the new integer literals (`wb` suffix)</del>

#### <del>bit-fields</del>

<del>Up to now, we don't have them at all in the book because they are very
confusing and only serve very specific needs. In C23, with bit-fields
of type `_BitInt(`*N*`)` we have a much more precise tool. Add as new
subsubsection 6.3.4.</del>

### <del>enumeration types<del>

#### <del>self-adjusting `enum` types to a suitable width<del>

- <del>adjust Takeaway 1.5.6.6, type of enumeration constants</del>
- <del>add an example for the return status of the multi-byte string functions</del>

#### <del>explicit specification of the underlying type</del>

- <del>introduce this new feature already in 5.6.2 and insist that it is
  better to fix the type.</del>
- <del>change occurences of plain `enum bla` to `enum bla : size_t` where we
  actually use the constants as lengths or similar</del>
- <del>clang-16 already has this, for gcc this will come with gcc13. Wait
  until this is released such that we can test the code.</del>


### <del>binary integer literals</del>

- <del>use `0b` binary constants wherever we are talking about bit-sets, in
  particular Tables 5.5 and 5.6</del>

- <del>adjust `printf` and `scanf` format discussion to `%b` and `%i`
  formats, in particular for bit patterns</del>

### <del>digit separators</del>

- <del>Use the new digit separator `'` throughout the book</del>
  - <del>for decimal and octal integers (are there any?), regroup three digits such as
    `18'446'744'073'709'551'615` and `056'777`</del>
  - <del>for decimal FP regroup three digits, if necessary include the
    decimal point inside the package. If the decimal point is at the
    boundary of a three digit package, omit the `'`
    separator. `3.14'159'265'358'979'323'846` and `70.000'001e-01`.</del>
  - <del>for hexadecimal integer regroup four digits that form a
    two-byte word,
    `0xBEAF'BEAF.`</del>
  - <del>Similar for hexadecimal float, but integrate the decimal point
    similar to decimal floating point, `0x1.9'99'99'99'99'99'9AP-3`
    and `0x19.99'99'99'99'99'9AP-4`</del>
  - <del>for binary integers regroup 8 digits that form a byte,
    `0b00000001'00011111`</del>
- <del>always write the prefixes (`0x` and `0b`) and exponents (`e` and
  `p`) with small letters and hexadecimal digits with capital
  letters</del>
- <del>mark leading `0x` and `0b` in bold</del>

<del>Add this policy to the style discussions in "Buckle up" and 9.1</del>

### <del>Endianess</del>

- <del>Talk about the new bit macros</del>
- <del>Add the macros to the fallback header.</del>

### <del>IO</del>

#### <del>binary `%b` and `0b`</del>

##### <del>input</del>

<del>Discuss binary input parsing (`strtoull` and similar) in 8.4. (Beware
the status for these functions concerning base 2, may still change,
depending on the WG14 reply to our NB comment.)</del>

<del>Also add `0b` and base 2 to the discussion in 14.1, Table 14.2</del>

##### <del>output</del>

- <del>add `%b` to table 3.1</del>
- <del>Table 5.6</del>
- <del>change discussion in 5.7.6 for sign representations</del>
- <del>change Takeaway 1.8.3.9 "bit patterns"</del>

#### <del>least width `%w`*N*</del>

<del>Add to discussion in 5.7.6, mainly replacing the use of `PRId64` and
similar macros.</del>

<del>Add these new specifiers to table 8.12</del>


#### <del>fast width `%wf`*N*</del>

<del>Add these new specifiers to table 8.12</del>

## <del>Change terminology from "constant" to "literal"</del>

<del>With the introduction of `constexpr` an important distinction now
becomes visible. When we introduce syntax terms we should talk about
*literals* not of *constants*. This concerns in particular 5.3 where
we should mostly replace *constant* by *literal* and add a footnote
that the standard calls them *constant*, unfortunately.</del>

<del>Only 5.3 for complex types should remain the same, because here
the standard does not define literals.</del>

<del>With the advent of `constexpr` also the need for object-like macros
should be drastically reduced. Take that change into account for 5.6.3
(Macros).</del>

## Floating point

C23 distinguishes binary (called "standard floating types") and
decimal floating types, but decimal floating types are optional. This
new distinction is very confusing because now we have to distinguish
this terminology properly. For example takeaway 1.5.3.9 talks about
"decimal floating-point constant" where now it should be "decimal
floating constant of standard floating type".

- Add a discussion of decimal floating types and the DF etc suffixes
  before 5.3.1 (complex constants)

- add a discussion of the representation of decimal floating point
  types to the end of 5.7.7, therefore use the new example code
  `decimal-binary.c`

- add the new format specifiers `H`, `D` and `DD` to tables 8.9 and 14.3

Perhaps deviate from the terminology of the standard and try
consistently to use "binary floating type" instead of "standard
floating type"


## <del>Pointers</del>

### <del>Use `nullptr` everywhere where appropiate.</del>

<del>integrate this into the explanation in 6.2, in particular takeaway
1.6.2.2</del>

<del>takeaway 2.11.1.10</del>

<del>adapt section 11.1.5, null pointers</del>

<del>provide fallback for those that are not yet at C23, see the file
`c23-fallback.h`</del>

<del>change takeaway 1.7.2.7 from "is `0`" to "is a null pointer".</del>

<del>change most uses of `0` in `fgets_manually` to `nullptr`.</del>


## <del>Attributes</del>

### <del>add a new subsection after 10.2.2 (pure functions)</del>

### <del>add to "Buckle up" (1)</del>

<del>Here we should use an example where the attribute binds to the left
just as `const`</del>

### <del>add `[[maybe_unused]]` to `argv` where possible</del>


<del>replace in the code snippets</del>

### <del>improve the discussion in section 15, performance</del>

- <del>use `[[unsequenced]]` and `[[reproducible]]` for a new subsection
  after 15.2, `restrict`</del>

### <del>add `[[fallthrough]]` to some `switch` labels</del>

<del>this is 3.3 for `switch`</del>

### <del>change `_Noreturn` to `[[noreturn]]`</del>

## <del>Program failure</del>

<del>Create a new section at the end of "Cognition"</del>

### <del>wrongdoings</del>

#### <del>replace "indeterminate value" by "indeterminate representation"</del>

#### <del>zero size re-allocations</del>

### <del>program state degradation</del>

### <del>unfortunate events</del>

#### <del>introduce `unreachable`<del>

#### <del>update the section on threads with a sub-section on deadlocks and livelocks</del>

### <del>unfortunate incidents</del>

### <del>series of unfortunate events</del>

### <del>dealing with failures<del>

### <del>Summary</del>

## <del>time interfaces</del>

<del>change the use of `asctime` and similar</del>

<del>mention new optional macros `TIME_MONOTONIC` etc</del>


## <del>type generic programming</del>

### <del>type inference and writing tg code</del>

- <del>adapt section 16, function-like macros, to type-generic proramming</del>

#### <del>introduce the new `auto` type inference feature</del>

#### <del>introduce the new `typeof` and `typeof_unqual` type inference features</del>

### <del>Add a section for compound expressions and lambdas</del>

### <del>`const`-preserving interfaces</del>

<del>discuss this in the corresponding sections about the C library,</del>
<del>provide fallback versions in the header file</del>

### <del>challenges for type-generic programming</del>

## security

### `memset_explicit`

### initialization

#### <del>introduce `{}` consistently everywhere</del>

<del>Use it in snippet after takeaway 1.6.2.3 instead of `0`. Then
introduce `nullptr` and to it with that (or the other way around)</del>

#### introduce command line options for implicit default initialization

#### dynamic initialization by means of `call_once`

add this to program termination (8.7)

## <del>Unicode</del>

### <del>consistent encoding for `u8`, `u` and `U` prefixes</del>

### <del>better treatment in *String Processing*</del>

### <del>write up the new rules for Unicode in identifiers</del>

<del>add a first π in the `constexpr` clause</del>

## <del>C library functions</del>

### <del>update the catalogues for the changes to `<math.h>` and `<tgmath.h>`</del>

### <del>introduce `strdup` and `strndup`</del>

### <del>add a chapter for integer operations before 8.2 (Mathematics)</del>

### <del>checked integer arithmetic `<stdckdint.h>`</del>

<del>also provide fallback code</del>

<del>add to the list of headers in table 8.1</del>

<del>write a small summary</del>

### <del>bit-fiddling `<stdbit.h>`</del>

<del>also provide fallback code</del>

<del>add to the list of headers in table 8.1</del>

<del>write a small summary</del>

## <del>Syntax</del>

### <del>Keywords</del>

<del>Some keywords now are changed from underscore versions (such as
`_Bool`) to normal versions (`bool`) for their principal use. Watch
that all of these changes are done.</del>

<del>`bool`, `false`, `true`, `alignof`, `alignas`,
`static_assert`, `thread_local`</del>



### <del>Default initialization with `{}`</del>

### <del>Adapt terminology used for scopes</del>

<del>streamline the use of the term "block"</del>

<del>in particular use things as "secondary block" consistently</del>




### <del>Labels</del>

### <del>Unnamed parameters</del>

<del>Add a new item in the first example that introduces the
feature.</del>

<del>*Go through all code examples and see where this applies. Done
does not seem to be too many.*</del>

### <del>pointers to arrays with qualifiers</del>

### <del>VLA parameters are mandatory</del>

### <del>VLA may be default initialized</del>

<del>Use that for the function `fibCache`</del>

### <del>storage class of compound literals</del>

### <del>forward declaration of parameters?</del>

<del>This one didn't make it into C23</del>

### <del>`constexpr` for name constants of any type</del>

<del>Add this as a new type of constant as its own new
subsection 5.6.5.</del>

<del>Adjust section 5.6.3, as there now *are* other means to define
constants than macros.</del>

### <del>conforming identifiers</del>

<del>Add a new section about "internationalization"</del>

## <del>preprocessing</del>

### <del>`#warning` directive</del>

### <del>`__has_include` operator</del>

### <del>querying attributes `__has_c_attribute`</del>

### <del>`#elifndef`</del>

### <del>comma omission and `__VA_OPT__` operator</del>

<del>Review examples for macros with optional arguments to use this new
feature, in particular in `<generic.h>` and `<macro_trace.h>`. This
also has us less rely on `0` beeing a replacement for `nullptr`.</del>

### <del>`#embed` and `__has_embed`</del>

<del>integrate into the section on binary data</del>

## General

### <del>change version to C23 in the introduction</del>

### refer to "Tiny C Projets" for platform specifities?

### Update <del>links</del> and references

### Add an appendix for C23 platforms

