\begin{thebibliography}{25}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Adams(1986)]{adams86:hitch_guide_galax}
Douglas Adams.
\newblock The hitchhiker's guide to the galaxy.
\newblock audiocassette from the double LP adaptation, 1986.
\newblock ISBN 0-671-62964-6.

\bibitem[C17()]{C17}
C17.
\newblock \emph{Programming languages - {C}}.
\newblock Number ISO/IEC 9899. ISO, fourth edition, 2018.
\newblock URL \url{https://www.iso.org/standard/74528.html}.

\bibitem[C23()]{C23}
C23.
\newblock \emph{Programming languages - {C}}.
\newblock Number ISO/IEC 9899. ISO, fifth edition, 2024.
\newblock URL \url{https://www.iso.org/standard/82075.html}.

\bibitem[Cormen et~al.(2001)Cormen, Leiserson, Rivest, and Stein]{cormen2001}
Thomas~H. Cormen, Charles~E. Leiserson, Ronald~L. Rivest, and Clifford Stein.
\newblock \emph{Introduction to Algorithms}.
\newblock MIT Press, 2 edition, 2001.

\bibitem[Dijkstra(1968)]{Dijkstra:1968:LEG:362929.362947}
Edsger~W. Dijkstra.
\newblock Letters to the editor: Go to statement considered harmful.
\newblock \emph{Commun. ACM}, 11\penalty0 (3):\penalty0 147--148, March 1968.
\newblock ISSN 0001-0782.
\newblock \doi{10.1145/362929.362947}.
\newblock URL \url{http://doi.acm.org/10.1145/362929.362947}.

\bibitem[Gardner(1970)]{gardner70}
Martin Gardner.
\newblock {Mathematical Games -- The fantastic combinations of John Conway's
  new solitaire game "life"}.
\newblock \emph{Scientific American}, 223:\penalty0 120--123, October 1970.

\bibitem[Gladman et~al.(2024)Gladman, Innocente, Mather, and
  Zimmermann]{gladman:hal-03141101}
Brian Gladman, Vincenzo Innocente, John Mather, and Paul Zimmermann.
\newblock {Accuracy of Mathematical Functions in Single, Double, Double
  Extended, and Quadruple Precision}.
\newblock working paper or preprint, February 2024.
\newblock URL \url{https://inria.hal.science/hal-03141101}.

\bibitem[Gustedt(2022)]{gustedt16-tg}
Jens Gustedt.
\newblock Improve type generic programming, January 2022.
\newblock URL \url{http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2890.pdf}.

\bibitem[{ISO/IEC/IEEE~60559}(2011)]{IEEE559}
{ISO/IEC/IEEE~60559}, editor.
\newblock \emph{Information technology -- Microprocessor Systems --
  Floating-Point arithmetic}, volume 60559:2011.
\newblock ISO, 2011.
\newblock URL \url{https://www.iso.org/standard/57469.html}.

\bibitem[Kernighan and Ritchie(1978)]{Kernighan78}
Brian~W. Kernighan and Dennis~M. Ritchie.
\newblock \emph{The {C} Programming Language}.
\newblock Prentice-Hall, Englewood Cliffs, New Jersey, 1978.

\bibitem[Knuth(1974)]{KnuthGoto}
Donald~E. Knuth.
\newblock Structured programming with go to statements.
\newblock In \emph{Computing Surveys}, volume~6. 1974.

\bibitem[Knuth(1997)]{knuth97:art1}
Donald~E. Knuth.
\newblock \emph{The Art of Computer Programming. Volume 1: Fundamental
  Algorithms}.
\newblock Addison-Wesley, 3rd edition, 1997.

\bibitem[Lamport(1978)]{Lamport78:time_clock_order}
Leslie Lamport.
\newblock Time, clocks and the ordering of events in a distributed system.
\newblock \emph{Communications of the ACM}, 21\penalty0 (7):\penalty0 558--565,
  1978.

\bibitem[Nishizeki et~al.(1977)Nishizeki, Takamizawa, and Saito]{NTS77}
T.~Nishizeki, K.~Takamizawa, and N.~Saito.
\newblock Computational complexities of obtaining programs with minimum number
  of {GO} {TO} statements from flow charts.
\newblock \emph{Trans. Inst. Elect. Commun. Eng. Japan}, 60\penalty0
  (3):\penalty0 259--260, 1977.

\bibitem[O'Donell and Sebor(2015)]{odonell15:annexk}
Carlos O'Donell and Martin Sebor.
\newblock Updated field experience with {A}nnex {K} -- bounds checking
  interfaces, September 2015.
\newblock URL \url{http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1969.htm}.

\bibitem[P\'{e}bay(2008)]{pebay08:stat-moments}
Philippe P\'{e}bay.
\newblock Formulas for robust, one-pass parallel computation of covariances and
  arbitrary-order statistical moments.
\newblock Technical Report SAND2008-6212, SANDIA, 2008.
\newblock URL
  \url{http://prod.sandia.gov/techlib/access-control.cgi/2008/086212.pdf}.

\bibitem[POSIX()]{POSIX2024}
POSIX.
\newblock \emph{IEEE/Open Group Standard for Information technology -- Portable
  Operating Systems Interface (POSIX\textsuperscript{\textregistered}) Base
  Specifications}, volume 1003.1-2024.
\newblock The Open Group, 2024.
\newblock Issue 8.

\bibitem[Ritchie(1993)]{Ritchie93}
Dennis~M. Ritchie.
\newblock The development of the {C} language.
\newblock In Thomas~J. Bergin, Jr. and Richard~G. Gibson, Jr., editors,
  \emph{Proceedings, ACM History of Programming Languages {I}{I}}, Cambridge,
  MA, April 1993.
\newblock URL \url{http://cm.bell-labs.com/cm/cs/who/dmr/chist.html}.

\bibitem[Sibidanov et~al.(2022)Sibidanov, Zimmermann, and
  Glondu]{sibidanov:hal-03721525}
Alexei Sibidanov, Paul Zimmermann, and St{\'e}phane Glondu.
\newblock {The CORE-MATH Project}.
\newblock In \emph{{ARITH 2022 - 29th IEEE Symposium on Computer Arithmetic}},
  pages 26--34, virtual, France, September 2022. {IEEE}.
\newblock \doi{10.1109/ARITH54963.2022.00014}.
\newblock URL \url{https://inria.hal.science/hal-03721525}.

\bibitem[Simonyi(1976)]{simonyi76:meta_programming}
Charles Simonyi.
\newblock Meta-programming: a software production model.
\newblock Technical Report CSL-76-7, {P}{A}{R}{C}, 1976.
\newblock URL
  \url{http://www.parc.com/content/attachments/meta-programming-csl-76-7.pdf}.

\bibitem[Thorup(1995)]{Thorup95structuredprograms}
Mikkel Thorup.
\newblock Structured programs have small tree-width and good register
  allocation.
\newblock \emph{Information and Computation}, 142:\penalty0 318--332, 1995.

\bibitem[Torvalds et~al.(1996)]{torvalds96:_linux}
Linus Torvalds et~al.
\newblock Linux kernel coding style, 1996.
\newblock URL
  \url{https://www.kernel.org/doc/Documentation/process/coding-style.rst}.
\newblock evolved mildly over the years.

\bibitem[Unicode(2017)]{Unicode}
Unicode, editor.
\newblock \emph{The Unicode Standard}.
\newblock The Unicode Consortium, Mountain View, CA, USA, 10.0.0 edition, 2017.
\newblock URL \url{https://unicode.org/versions/Unicode10.0.0/}.

\bibitem[von Neumann(1945)]{vonNeumann1945}
John von Neumann.
\newblock First draft of a report on the {EDVAC}, 1945.
\newblock internal document of the ENIAC project.

\bibitem[Welford(1962)]{welford62}
B.~P. Welford.
\newblock Note on a method for calculating corrected sums of squares and
  products.
\newblock \emph{Technometrics}, 4\penalty0 (3):\penalty0 419--420, 1962.

\end{thebibliography}
