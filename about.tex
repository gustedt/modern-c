\startonoddpage
\section*{About this book}
%  \subsection*{Preliminaries}
  The C programming language has been around since the early 1970s
  (see~\cite{Ritchie93}). Since then, C has been used in an incredible
  number of applications. Programs and systems written in C are all around
  us in personal computers, phones, cameras, set-top boxes, refrigerators,
  cars, mainframes, satellites---basically any modern device that has
  a programmable interface.

  In contrast to the ubiquitous presence of C programs and systems, good
  knowledge of and about C is much more scarce. Even experienced C
  programmers often appear to be stuck in some degree of self-inflicted
  ignorance about the modern evolution of the C language. A likely reason
  for this is that C is seen as an "easy to learn" language, allowing a
  programmer with little experience to quickly write or copy snippets of
  code that at least appear to do what it's supposed to. In a way, C fails
  to motivate its users to climb to higher levels of knowledge.

  This book is intended to change that general attitude, so it is
  organized in \emph{levels} that reflect familiarity with the C language
  and programming in general. This structure may go against some habits
  of the book's readers. In particular, it splits some difficult subjects (such as
  pointers) across levels so as not to swamp readers too early
  with the wrong information. We'll explain the book's organization in more detail shortly.

Generally, although many universally applicable ideas will be presented
that would also be valid for other programming languages (such as Java,
Python, Ruby, C\#, and C++), the book primarily addresses concepts and
practices that are unique to C or are of particular value when programming
in the C language.

\subsection*{C revisions}

As the title of this book suggests, today's C is not the same language as
the one originally designed by its creator. Right from the start, C has
been in a continuous process of adjustment and improvement. Usually, early
C is referred to as K\&R C (Kernighan and Ritchie C) after the first book
that made the language popular~(\cite{Kernighan78}).
Since then, it has undergone an
  important standardization and extension process, now driven by the
  International Standards Organization (ISO). This led to the
  publication of a series of C standards in 1989, 1999, 2011, 2018, and 2024, commonly
  referred to as \C{89}, \C{99}, \C{11}, \C{17}, and \C{23}. The C standards committee put a lot of
  effort into guaranteeing backward compatibility such that code written
  for earlier revisions of the language (say, \C{11}), should compile to a
  semantically equivalent executable with a compiler that implements a
  newer revision. Unfortunately, this backward compatibility has had the
  unwanted side effect of not motivating projects that could benefit
  greatly from the new features to update their code base. To emphasize this
  progress of revisions, we indicate which standard
  revision introduced newer features.

  \subsection*{This edition}

  This edition presents a considerable rework given the latest
  revision, \C{23}, of the C standard. A lot of new material has been added,
  and many expositions have been straightened out to reflect the new
  capabilities of the C programming language. So, in this book, we will
  mainly refer to \C{23}, as defined in~\cite{C23}, but at the time of this
  writing, compilers hadn't yet implemented this standard completely. If you
  want to compile the examples in this book, you will need at least a
  compiler that implements most of \C{17}. For the novelties that \C{23}
  introduces, we provide a compatibility header and discuss how to possibly
  generate a suitable C~compiler and C~library platform on POSIX systems as
  a fallback in technical annex~\ref{level:annex}. Beware that this is not
  meant as a permanent tool but as only a crutch while platforms adapt.

  \subsection*{C and C++}

Programming has become a very important cultural and economic activity,
  and C remains an important element in the programming world. As in all
  human activities, progress in C is driven by many factors, including corporate or
  individual interest, politics, beauty, logic, luck, ignorance,
  selfishness, ego, sectarianism, and (add your primary motivation here). Thus,
  the development of C has not been and cannot be ideal. It has flaws and
  artifacts that can only be understood within their historical and societal
  context.

  An important part of the context in which C developed was the early
  appearance of its sister language, C++. One common misconception is that
  C++ evolved from C by adding its particular features. Although this is
  historically correct (C++ evolved from a very early C), it is not
  particularly relevant today. In fact, C and C++ separated from a common
  ancestor more than 30 years ago and have evolved separately ever
  since. But this evolution of the two languages has not taken place in
  isolation; they have exchanged and adopted each other's concepts over the
  years. Some new features, such as the addition of atomics and
  threads, have been designed in close collaboration between the C and C++
  standard committees.

  Nevertheless, many differences remain, and generally, all that is said in
  this book is about C, not C++. Many of the code examples will
  not even compile with a C++ compiler. So we should not mix sources of
  both languages.
\begin{reminder}
  C and C++ are different: don't mix them, and don't mix them up.
\end{reminder}

Note that when you are working through this book, you will encounter many lines marked like
that one. These takeaways summarize features, rules,
recommendations, and so on. There is a list of these
  takeaways toward the end of the book, which you might use as a cheat
sheet.

\subsection*{Requirements}

  To be able to profit from this book, you need to fulfill some minimal
  requirements. If you are uncertain about any of these, please obtain or
  learn them first; otherwise, you might waste a lot of time.

  First, you can't learn a programming language without practicing it, so
  you \emph{must} have a decent programming environment at your disposal
  (usually on a PC or laptop), and you \emph{must} master it to some
  extent. This environment can be integrated (an IDE) or
  a collection of separate utilities. Platforms vary widely in what they
  offer, so it is difficult to advise on specifics. On Unix-like
  environments such as Linux and Apple's macOS, you will find editors
  such as
% JG: capitalization (or not) is important such that readers may find
% these tools on their platform. Leave it like that. 
  \texttt{emacs} and \texttt{vim} and compilers such as \texttt{c99}, \texttt{c17},
  \texttt{gcc}, and \texttt{clang}.

  You must be able to do the following:
  \begin{enumerate}
  \item Navigate your file system. File systems on computers are usually
    organized hierarchically in \emph{directories}. You must be able to
    navigate through these to find and manipulate files.
  \item Edit programming text. This is different from editing a letter in
    a word-processing environment. Your environment, editor, or whatever it
    is called should have a basic understanding of the programming language
    C. You will see that if you open a C file (which usually has the file extension
    \texttt{.c}). It might highlight some keywords or help you indent your
    code according to the nestedness of \texttt{\{\}} brackets.
  \item Execute a program. The programs you will see here are very
    basic at first and will not offer you any graphical features. They need
    to be launched in the \emph{command line}. An example of such a program
    launched that way is the \emph{compiler}. On Unix-like environments, the command line is usually
    called a \emph{shell} and is launched in a \emph{console} or
    \emph{terminal}.
  \item Compile programming text. Some environments provide a menu
    button or a keyboard shortcut for compilation. An alternative
    is to launch the compiler in the command line of a
    terminal.
% JG: Emphasize is important here, don't change.
    This compiler \emph{must} adhere to recent standards; don't
    waste your time with a compiler that does not conform.
  \end{enumerate}

  If you have never programmed before, this book will be tough. Knowing some of
  the following will help:
  %
  Basic,
  C (historical revisions),
  C++,
  Fortran,
  R,
  bash,
  JavaScript,
  Java,
  MATLAB,
  Perl,
  Python,
  Scilab,
  %
  and so on.
  %
  But perhaps you have had some other programming experience,
  maybe even without noticing. Many technical specifications actually come in
  some sort of specialized language that can be helpful as an analogy---
  for example, \emph{HTML} for web pages and \emph{LaTeX} for document
  formatting.

  You should have an idea of the following concepts, although their precise
  meanings may be a bit different in C than in the context
  where you learned them:

  \medskip
  \begin{description}
  \item[\emph{Variables}] Named entities that hold values
  \item[{Conditionals}] Doing something (or not) subject to a precise
    condition
  \item[\emph{Iteration}] Doing something repeatedly for a specified number of
    times or until a certain condition is met
  \end{description}

\subsection*{Source code}
Many of the programming code snippets presented in this book are
publicly available (see
\url{https://inria.hal.science/hal-03345464/document}).
This allows you to view
them in context and to compile and try them out. The archive also
contains a \texttt{Makefile} with a description of the components that
are needed to compile these files. It is centered around Linux
or, more generally, POSIX systems, but it may also help you to find out
what you need when you are on a different system.

\subsection*{Exercises and challenges}
Throughout this book, you'll see exercises
meant to get you thinking about the concepts being discussed.
These are probably best done directly along with your reading. Then
there is another category called ``challenges.'' These are generally more
demanding. You will need to do some research even to understand what they are about,
and the solutions will not come all by themselves; they will require effort. They will
take more time, sometimes hours or, depending on your
degree of satisfaction with your work, even days. The subjects covered
in these challenges are the fruit of my own personal bias toward ``interesting
questions'' from my personal experience.
If you have other problems or projects in
your studies or work that cover the same ground, they should do equally
well. The important aspect is to train yourself by first searching for help
and ideas elsewhere and then to get your hands dirty and get things
done. You will only learn to swim if you jump into the water.

%\clearpage
\subsection*{Organization}
This book is organized in \emph{levels}, numbered from~\ref{level:magpie}
to~\ref{level:chough}. The starting level~\ref{level:magpie}, named
Encounter, will summarize the very basics of programming with C. Its
principal role is to remind you of the main concepts we have
mentioned and familiarize you with the special vocabulary and viewpoints
that C applies.%
\footnote{%
  One of C's special viewpoints is that indexing starts at \code{0} and
  not at \code{1} as in Fortran.%
} %
By the end of it, even if you don't have much experience in programming
with C, you should be able to understand the structure of simple C programs
and start writing your own.

The Acquaintance level~\ref{level:raven} details most principal concepts and features
such as control structures, data types, operators, and functions. It should
give you a deeper understanding of what is going on when you
run your programs. This knowledge should be sufficient for an introductory
course in algorithms and other work at that level, with the notable caveat
that pointers are not yet fully introduced.


The Cognition level~\ref{level:jay} goes to the heart of the C language. It fully
explains pointers, familiarizes you with C's memory model, and allows you
to understand most of C's library interface. Completing this level should
enable you to write C code professionally; it therefore begins with an
essential discussion about the writing and organization of C programs. I
personally would expect anybody who graduated from an engineering school
with a major related to computer science or programming in C to master this
level. Don't be satisfied with less.

The Experience level~\ref{level:chough} then goes into detail about specific topics, such as
performance, reentrancy, atomicity, threads, and type-generic
programming. These are probably best discovered as you go, which is when you
encounter them in the real world. Nevertheless, as a whole, they are
necessary to round off the discussion and to provide you with full expertise
in C. Anybody with some years of professional programming in C or who heads
a software project that uses C as its main programming language should
master this level.

\subsection*{Author}

\begin{wrapfigure}{r}{3cm}
  \hfill\\[2ex]
  \includegraphics[width=3cm]{fig/Jens_Gustedt_2024}\\[-3ex]
\end{wrapfigure}
Jens Gustedt completed his studies in mathematics at the University of Bonn
and Berlin Technical University. His research at that time covered the
intersection between discrete mathematics and efficient computation. Since
1998, he has been working as a senior scientist at the French National Institute
for Computer Science and Control (INRIA), first in the LORIA lab, Nancy,
and, since 2013, in the ICube lab, Strasbourg, where he has been a
deputy director since 2023.

Throughout his career, most of his scientific research has been accompanied
by the development of software---at the beginning, mostly in C++ and then
later exclusively in C. He serves AFNOR (the French standards
organization) as an expert on the ISO committee JTC1/SC22/WG14. He was
co-editor for the C~standard document~\cite{C17} and for the initial phase
of~\cite{C23}. He also has a successful blog that deals with programming in
C and related topics (\url{https://gustedt.wordpress.com}).
