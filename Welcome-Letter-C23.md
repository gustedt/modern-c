Thank you for purchasing the MEAP edition of Modern C, **`C23`**
Edition.

After over 40 years of development, C is still among the major
programming languages, ubiquitously used in basically all modern
electronic devices, ranging from a small music chip in a birthday
greeting card, your refrigerator, your phone, your laptop, to
mainframes or satellites. This book is meant to provide you a leveled
approach in learning and mastering C. The previous edition already has
been largely successful and is considered by some as one of the
reference books on the subject.

This new edition has been the occasion to overhaul the presentation in
many places, but its main purpose is the update to the new C standard,
**`C23`**. The goal is to publish this new edition of Modern C at the
same time as the new C standard goes through the procedure of ISO
publication and as new releases of major compilers will implement all
the new features that it brings.

Among the most noticeable changes and additions that we handle are
those for integers: there are new bit-precise types coined
**`_BitInt`**(N), new C library headers `<stdckdint.h>` (for
arithmetic with overflow check) and `<stdbit.h>` (for bit
manipulation), possibilities for 128 bit types on modern
architectures, and substantial improvements for enumeration types.

Other new concepts in C23 include a `nullptr` constant and its
underlying type, syntactic annotation with attributes, more tools for
type generic programming such as type inference with `auto` and
`typeof`, default initialization with `{}`, even for variable length
arrays. and `constexpr` for name constants of any type.  Furthermore,
new material has been added, discussing compound expressions and
lambdas, so-called "internationalization", a comprehensive approach
for program failure.

During MEAP we will also add an appendix and a temporary include
header for an easy transition to C23 on existing platforms, that will
allow you to start off with **`C23`** right away.


