\subsection{Function pointers}
\label{sec:function-pointers}

\index{function pointer@pointer!function} There is yet another construct
for which the address-of operator \code{&} can be used: functions. We
saw this concept pop up when discussing the \code{atexit}
function (section~\ref{sec:program-termination}), which is a function that receives a
function argument. The rule is similar to that for array decay, which we described
earlier:
\begin{reminder}[function decay]
  A function name without following parenthesis decays to a pointer to its
  start.
  \label{function-pointer-decay}
\end{reminder}

Syntactically, functions and function pointers are also similar to arrays in
type declarations and as function parameters:
\begin{shortlisting}
  typedef void atexit_function(void);
  // Two equivalent definitions of the same type, which hides a pointer
  typedef atexit_function* atexit_function_pointer;
  typedef void (*atexit_function_pointer)(void);
  // Five equivalent declarations for the same function
  void atexit(void f(void));
  void atexit(void (*f)(void));
  void atexit(atexit_function f);
  void atexit(atexit_function* f);
  void atexit(atexit_function_pointer f);
\end{shortlisting}

Which of the semantically equivalent ways of writing the function declaration
is more readable could certainly be the subject of much debate. The second
version, with the \code{(*f)} parentheses, quickly gets difficult to read, and
the fifth is frowned upon because it hides a pointer in a type. Among the
others, I personally slightly prefer the fourth over the first.

The C library has several functions that receive function parameters. We
have seen \code{atexit} and \code{at_quick_exit}. %
{\lstindexbf Another pair of functions in \Cinclude{stdlib.h} provides
  generic interfaces for searching ({\cfuncstyle bsearch}) and sorting
  (\code{qsort}):%
}
\begin{shortlisting}
typedef int compare_function(void const*, void const*);

void* /*@{\cfuncstyle bsearch}*/(void const* key, void const* base,
              size_t n, size_t size,
              compare_function* compar);

void qsort(void* base,
           size_t n, size_t size,
           compare_function* compar);
\end{shortlisting}

Both receive an array \code{base} as an argument on which they perform their
task. The address to the first element is passed as a \code{void} pointer,
so all type information is lost. To be able to handle the array properly,
the functions have to know the size of the individual elements
(\code{size}) and the number of elements (\code{n}).

In addition, they receive a comparison function as a parameter that provides
the information about the sort order between the elements. By using such a
function pointer, the {\cfuncstyle bsearch} and \code{qsort} functions are very
generic and can be used with any data model that allows for an ordering of
values. The elements referred by the \code{base} parameter can be of any type
\code{T} (\code{int}, \code{double}, string, or application defined) as long
as the \code{size} parameter correctly describes the size of \code{T} and
as long as the function pointed to by \code{compar} knows how to compare
values of type \code{T} consistently.

A simple version
of such a function would look like this:
\begin{shortlisting}
int compare_unsigned(void const* a, void const* b){
  unsigned const* A = a;
  unsigned const* B = b;
  if (*A < *B) return -1;
  else if (*A > *B) return +1;
  else return 0;
}
\end{shortlisting}

The convention is that the two arguments point to elements that are to be
compared, and the return value is strictly negative if \code{a} is
considered less than \code{b}, \code{0} if they are equal, and strictly
positive otherwise.

The return type of \code{int} seems to suggest that \code{int} comparison
could be done more simply:
\begin{shortlisting}
/* An invalid example for integer comparison */
int compare_int(void const* a, void const* b){
  int const* A = a;
  int const* B = b;
  return *A - *B;    // may overflow!
}
\end{shortlisting}
But this is not correct. For example, if \code{*A} is big, say
\code{INT_MAX}, and \code{*B} is negative, the mathematical value of the
difference can be larger than \code{INT_MAX}.

Because of the \code{void} pointers, a usage of this mechanism should
always take care that the type conversions are encapsulated similar to the
following:
\begin{shortlisting}
/* A header that provides searching and sorting for unsigned. */

/* No use of inline here; we always use the function pointer. */
extern int compare_unsigned(void const*, void const*);

inline
unsigned const* bsearch_unsigned(unsigned const key[static 1],
                        size_t nmeb, unsigned const base[nmeb]) {
   return /*@{\cfuncstyle bsearch}*/(key, base, nmeb, sizeof base[0], compare_unsigned);
}

inline
void qsort_unsigned(size_t nmeb, unsigned base[nmeb]) {
   qsort(base, nmeb, sizeof base[0], compare_unsigned);
}
\end{shortlisting}
Here, {\cfuncstyle bsearch} (binary search) searches for an element that compares
equal to \code{key[0]} and returns it or returns a \plainindex{null pointer} if no such element is
found. It supposes that array \code{base} is already sorted consistently to
the ordering that is given by the comparison function. This assumption
helps speed up the search. Although this is not explicitly specified in
the C standard, you can expect that a call to {\cfuncstyle bsearch} will never
make more than $\left\lceil\log_2(n)\right\rceil$ calls to \code{compar}.

If {\cfuncstyle bsearch} finds an array element that is equal to
\code{*key}, it returns the pointer to this element. Note that this, if
used like that, drills a hole in C's type system, since this returns an
unqualified pointer to an element whose \plainindex[effective
type]{effective type} might be \code{const}-qualified. Use with care. In
our example, we simply convert the return value to \code{unsigned}
\tcode{const*}, such that we will never even see an unqualified pointer at
the call side of \code{bsearch_unsigned}. Since \C{23} {\cfuncstyle
  bsearch} has in fact been upgraded to a type-generic macro \code{bsearch}
of the same name, see~\ref{tg-library}, which circumvents this flaw.

The name \code{qsort} is derived from the \emphindex{quick sort}
algorithm. The standard doesn't impose the choice of the sorting algorithm,
but the expected number of comparison calls should be of the magnitude of
$n\log_2(n)$, just like quick sort. There are no guarantees for upper
bounds; you may assume that its worst-case complexity is at most
quadratic, $O(n^2)$.

Whereas there is a catch-all pointer type, \code{void*} that
can be used as a generic pointer to object types, no such generic type or
implicit conversion exists for function pointers.
\begin{reminder}
  Function pointers must be used with their exact type.
\end{reminder}
Such a strict rule is necessary because the calling conventions for
functions with different prototypes may be quite different\footnote{The
  platform application binary interface (ABI) may,
  for example, pass floating points in special hardware
  registers.} and the pointer itself does not keep track of any of this.

The following function has a subtle problem because the types of the
parameters are different than what we expect from a comparison function:
\begin{shortlisting}
/* Another invalid example for an int comparison function */
int compare_int(int const* a, int const* b){
  if (*a < *b) return -1;
  else if (*a > *b) return +1;
  else return 0;
}
\end{shortlisting}

When you try to use this function with \code{qsort}, your compiler should
complain that the function has the wrong type. The variant that we gave
earlier using intermediate \code{void} \tcode{const*} parameters should be almost as
efficient as this invalid example, but it also can be guaranteed to be
correct on all C platforms.


\bigskip
\emph{Calling} functions and function pointers with the \code{(...)}
operator has rules similar to those for arrays and pointers and the
\code{[...]} operator:

\begin{shortlisting}
  double f(double a);

  // Equivalent calls to f, steps in the abstract state machine
  f(3);        // Decay → call
  (&f)(3);     // Address of → call
  (*f)(3);     // Decay → dereference → decay → call
  (*&f)(3);    // Address of → dereference → decay → call
  (&*f)(3);    // Decay → dereference → address of → call
\end{shortlisting}

\begin{reminder}
  The function call operator \tcode{(}$\cdots$\tcode{)} applies to function pointers.
\end{reminder}

So, technically, in terms of the abstract state machine,
\index{state!abstract}%
\index{abstract state}%
the pointer decay
is always performed, and the function is called via a function pointer. The
first, ``natural'' call has a hidden evaluation of the \code{f} identifier
that results in the function pointer.

Given all this, we can use function pointers almost like functions:
\begin{shortlisting}
// In a header
typedef int logger_function(char const*, ...);
extern logger_function* logger;
enum logs { log_pri, log_ign, log_ver, log_num };
\end{shortlisting}
This declares a global variable \code{logger} that will point to a function
that prints out logging information. Using a function pointer will allow
the user of this module to choose a particular function dynamically:
\begin{shortlisting}
// In a .c file (TU)
extern int logger_verbose(char const*, ...);
static
int logger_ignore(char const*, ...) {
  return 0;
}
logger_function* logger = logger_ignore;

static
logger_function* loggers = {
  [log_pri] = printf,
  [log_ign] = logger_ignore,
  [log_ver] = logger_verbose,
};
\end{shortlisting}
Here, we are defining tools that implement this approach. In particular,
function pointers can be used as a base type for arrays (here,
\code{loggers}). Observe that we use two external functions (\code{printf}
and \code{logger_verbose}) and one \code{static} function
(\tcode{logger_}\\\tcode{ignore}) for the array initialization; the storage
class is not part of the function interface.

The \code{logger} variable can be assigned just like any other pointer
type. Somewhere at startup, we can have
\begin{shortlisting}
if (LOGGER < log_num) logger = loggers[LOGGER];
\end{shortlisting}
Then this function pointer can be used anywhere to call the
corresponding function:
\begin{shortlisting}
logger("Do we ever see line \%lu of file \%s?", __LINE__+0UL, __FILE__);
\end{shortlisting}

This call uses the special macros \code{__LINE__} and \code{__FILE__} for
the line number and the name of the source file. We will discuss these in
more detail in subsection~\ref{sec:macro-tracing}.

When using pointers to functions, you should always be aware that doing so introduces an
indirection to the function call. The compiler first has to fetch the
contents of \code{logger} and can only then call the function at the
address it found there. This has a certain overhead and should be
avoided in time-critical code.

\vfil
\begin{exercise}[Generic derivative]
  \label{exs:generic-derivative}
  Can you extend the real and complex derivatives
  (challenges~\ref{exs:numerical} and~\ref{exs:complex}) such that they
  receive the function \code{F} and the value \code{x} as a parameter?

  Can you use the generic real derivatives to implement Newton's method for
  finding roots?

  Can you find the real zeros of polynomials?

  Can you find the complex zeros of polynomials?
\end{exercise}

\vfil
\begin{exercise}[Generic sorting]
  \label{exs:sorting-generic}

  Can you extend your sorting algorithms (challenge~\ref{exs:sorting-algo})
  to other sort keys?

  Can you condense your functions for different sort keys to functions that
  have the same signature as \code{qsort}---that is, receive generic pointers
  to data, size information, and a comparison function as parameters?

  Can you extend the performance comparison of your sorting algorithms
  (challenge~\ref{exs:sorting-performance}) to the C library function
  \code{qsort}?

\end{exercise}

%%% Local Variables:
%%% mode: latex
%%% eval: (reftex-mode)
%%% TeX-master: "modernC.tex"
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
