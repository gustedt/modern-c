\subsection{Unsequenced and reproducible attributes}
\label{sec:unsequenced}

{\lstindexbf%
  Many of our example functions use the attribute
  \code{[[unsequenced]]} (or the header-safe form
  \code{[[__unsequenced__]]}) to indicate a function is pure.

\begin{reminder}
  All pure functions should have the attribute \code{[[unsequenced]]}.
\end{reminder}
}

Other than before for \code{restrict}, such an annotated \emph{pure}
function can usually be used freely without restrictions. If the
function definition is verified,
this knowledge can be used to optimize the call site much better.

{\lstindexbf%
For the first part of this section, we will assume that situation---
namely, that the attributed function is, in fact, pure. Later, we will see
how these definitions extend to functions that have pointer arguments
or return values and that may have an internal state for
the \code{[[reproducible]]} attribute.

So here, the burden of verification is on the writer of the function
definition. In general, for the simple case of pure functions, this is
not very difficult: we have to ensure that the function only depends
on its arguments and has no other effects other than returning a
value. This splits into several properties that are relatively easy to
check.

For the state dependency part, it is formulated as follows.

\begin{reminder}
  A function with the attribute \code{[[unsequenced]]} shall not read
  nonconstant global variables or system state.
\end{reminder}

Here, the possible danger does not only lie in global variables that
are read and would possibly change under our feet. There are other
more subtle parts of the execution state on which a function may rely
that we may not easily notice. A good example of such an implicit
state is the floating point rounding mode. Unfortunately, in some
parts, the C~library follows a quite antiquated model for tuning the
floating point model: a thread-local state is modified by the use of
pragmas:

\begin{shortlisting}
  #pragma FP_CONTRACT OFF
  #pragma FENV_ROUND FE_TONEAREST
\end{shortlisting}

Here, for example, this sets the global thread-local state such that
no floating-point expression contractions are performed and
the result of rounding always goes to the nearest representable
number. Since this state is accessible by programming interfaces,
users of a function that we think is pure may indeed depend on a
state that changes between different calls.

\begin{reminder}
  In general, a function that uses floating point arithmetic is not
  pure and shall not have the attribute \code{[[unsequenced]]}.
\end{reminder}

}

This statement, in particular, includes all functions in the
\Cinclude{math.h}, \Cinclude{tgmath.h} and\hfill\\
\Cinclude{complex.h}
headers. What a disappointment, I hear you say, but please be
patient; there is at least a partial cure in the following discussion. However, for the moment,
things will even get worse.

\begin{reminder}
  A function with the attribute \code{[[unsequenced]]} shall not apply
  visible modifications to global variables or system states.
\end{reminder}

Here again, many C~library functions are not admissible because they
use another antiquated model that uses global state: \code{errno}.

\begin{reminder}
  A function that returns possible errors through \code{errno} is not
  pure and shall not have the attribute \code{[[unsequenced]]}.
\end{reminder}

{\lstindexbf%
  Fortunately, there is a remedy to work around this that uses the pragmas
  \code{FP_CONTRACT} and \code{FENV_ROUND}. Here is an example from the
  original paper that proposed this feature for \C{23}:%
  \footnote{%
    É. Alepins, J. Gustedt, \emph{Unsequenced functions},
    https://open-std.org/JTC1/SC22/WG14/www/docs/n2956.htm, 2022.%
  }%
}

\begin{shortlisting}
#include <math.h>
#include <fenv.h>
inline double distance (double const x[restrict static 2]) [[reproducible]] {
#   pragma FP_CONTRACT OFF
#   pragma FENV_ROUND FE_TONEAREST
    // We assert that sqrt will not be called with invalid arguments
    // and the result only depends on the argument value.
    extern double sqrt(double) [[unsequenced]];
    return sqrt(x[0]*x[0] + x[1]*x[1]);
}
\end{shortlisting}

\noindent%
Here, we manually make some guarantees that \code{sqrt} otherwise does not
fulfill in all cases:
\begin{itemize}
\item In that context, the function is never called with negative arguments
  because a sum of squares is always positive. So, here, all calls are valid,
  and \code{errno} is never set.
\item For the scope of the \code{distance} function, the two pragmas
  are always set to the same state, so the call to \code{sqrt} always
  sees the same state.
\end{itemize}

\medskip%
\noindent%
In this special context, \code{sqrt} is indeed pure and may be
annotated with the attribute. The mechanisms in place that
guarantee this works are twofold.

\begin{reminder}
  Pragmas that change the floating point state act locally within the
  current scope.
\end{reminder}

In our example, that means that the rounding mode possibly changes at
the position of the pragma and then switches back to the original
value at the end of the scope in which the pragma is placed.

\begin{reminder}
  Type attributes accumulate within the current scope.
\end{reminder}

Here, that means as long as we are inside \code{distance}, the compiler
sees \code{sqrt} with the attribute and may use this information to
optimize locally. Usages of the function in other contexts are not
affected.

{\lstindexbf%
  Our function \code{distance} bears another attribute:
  \code{[[reproducible]]}. As the name indicates, the idea is that we
  only have to guarantee that the result(s) and effects of such a call with
  a given set of arguments are always the same, regardless of how the function
  achieves this internally. This attribute has weaker prerequisites and,
  thus, fewer optimization opportunities. Our function clearly has a property
  that disqualifies it from being unsequenced: it changes the global state---
  namely, the floating point properties.

  \begin{reminder}
    A function with the attribute \code{[[reproducible]]} may
    temporarily modify the global state as long as it restores it to its
    original value.
  \end{reminder}

}

Also, \code{distance} receives a vector of two elements, \code{x}, as
a pointer parameter, so it also isn't pure in the classical sense, as we
have seen previously. Nevertheless, the two attributes extend the classical
notions such that they can be applied to functions with pointer parameters
and pointer returns, as long as these fulfill the same requirements as
\code{restrict}-qualified pointer parameters.

\begin{reminder}
  For functions with \code{[[unsequenced]]} and \code{[[reproducible]]}
  attribute annotate pointer parameters with \code{restrict}.
\end{reminder}

Adding this qualification is redundant. The attributes themselves already
impose the associated properties, but an explicit annotation helps to
remind the occasional reader of the restrictions.


Now, in the same spirit as giving local guarantees for
\code{sqrt}, in a context where we know that we don't change the floating
point environment, we can also improve the local knowledge about
\code{distance}.

\begin{shortlisting}
double g (double y[static 1], double const x[static 2]) {
  // We assert that distance will not see different states of the floating
  // point environment.
  extern double distance (double const x[restrict static 2]) [[unsequenced]];
  y[0] = distance(x);
  ...
  return distance(x);   // Replacement by y[0] is valid.
}
\end{shortlisting}

For both attributes, we also have to consider the internal state that could be
changed by such a function---namely, local objects that have static storage
duration.

\begin{reminder}
  A function with the attribute \code{[[unsequenced]]} shall not modify the
  local static state, even through other function calls.
\end{reminder}

With all these requirements, several calls to \code{[[unsequenced]]}
functions can even be interleaved and executed without changing the final
results. For \code{[[reproducible]]}, this requirement is also a bit
relaxed.

\begin{reminder}
  A function with the attribute \code{[[reproducible]]} shall only modify
  the local static state if that state is not observable from outside the
  function.
\end{reminder}

To see the difference, consider an interface with the following function
\code{hash}.

\begin{shortlisting}
size_t hash(char const[restrict static 32]) [[reproducible]];
\end{shortlisting}

\noindent%
Such a function is supposed to return the same integer value if called with
the same string argument. Nevertheless, it may memorize results for
arguments already seen internally in a table declared with
\code{static}. Thereby, repeated calls with the same string may be more
efficient, whereas the visible state outside of the function shows no
observable difference. Nevertheless, such a function cannot have an
\code{[[unsequenced]]} attribute because interleaving calls would not be
possible; calls to \code{hash} must always be properly sequenced, one after
another.

%%% Local Variables:
%%% mode: latex
%%% eval: (reftex-mode)
%%% fill-column: 75
%%% ispell-dictionary: "american"
%%% x-symbol-8bits: nil
%%% End:
